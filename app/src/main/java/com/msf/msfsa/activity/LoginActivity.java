package com.msf.msfsa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.msf.msfsa.R;
import com.msf.msfsa.application.MyApplication;
import com.msf.msfsa.models.Users;
import com.msf.msfsa.models.gson.GetUsers;
import com.msf.msfsa.network.EndPoints;
import com.msf.msfsa.services.BackgroundLocationService;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.json.JSONObject;

/**
 * Created by Kamil on 3/28/2016.
 */
public class LoginActivity extends AppCompatActivity {
    TextView username, password;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        username = (TextView) findViewById(R.id.username);
        password = (TextView) findViewById(R.id.password);
        if (MyApplication.getInstance().getPrefManager().getUserstatus()) {
            Intent myIntent = new Intent(this, BackgroundLocationService.class);
            startService(myIntent);
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    public void onliginclick(View view) {
        if (isValid()) {
            dologin();
        }
    }

    boolean isValid() {
        if (username.getText().length() == 0) {
            username.setError("username invalid");
            return false;
        }
        if (password.getText().length() == 0) {
            password.setError("password invalid");
            return false;
        }
        return true;
    }

    void dologin() {
        if (Users.listAll(Users.class).size() == 0) {
            JsonObjectRequest user = new JsonObjectRequest(EndPoints.USERS, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                Log.e("get user:", response.toString());
                    Gson gson = new Gson();
                    GetUsers userslist = gson.fromJson(response.toString(), GetUsers.class);
                    userslist.save();
                    login();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    Log.e("login", "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                    Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            MyApplication.getInstance().addToRequestQueue(user);
        } else
            login();
    }

    void login() {
        long count = Select.from(Users.class)
                .where(Condition.prop("usercode").eq(username.getText().toString()),
                        Condition.prop("userpassword").eq(password.getText().toString())).count();
        if (count == 0) {
            Toast.makeText(getApplicationContext(), "username or password incorrect", Toast.LENGTH_SHORT).show();
        } else {
            MyApplication.getInstance().getPrefManager().setUserstatus(true);
            MyApplication.getInstance().getPrefManager().setUserId(Integer.valueOf(username.getText().toString()));
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}
