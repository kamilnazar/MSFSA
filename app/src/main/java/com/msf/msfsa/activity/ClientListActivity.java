package com.msf.msfsa.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.msf.msfsa.R;
import com.msf.msfsa.adapter.ClientListAdapter;
import com.msf.msfsa.models.Clients;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 3/28/2016.
 */
public class ClientListActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    List<Clients> clientsList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private ClientListAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.client_list);

        int activityid = getIntent().getExtras().getInt("activity_id");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);


        clientsList = Clients.listAll(Clients.class);
//        List<Questions> q = Questions.listAll(Questions.class);
        Log.d("client count:", String.valueOf(clientsList.size()));

        mAdapter = new ClientListAdapter(this, clientsList, activityid);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {
        final List<Clients> filteredModelList = filter(clientsList, query);
        mAdapter.animateTo(filteredModelList);
        mRecyclerView.scrollToPosition(0);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<Clients> filter(List<Clients> models, String query) {
        query = query.toLowerCase();

        final List<Clients> filteredModelList = new ArrayList<>();
        for (Clients model : models) {
            final String code = model.getClient_Code().toLowerCase();
            final String name = model.getClient_Description().toLowerCase();
            if (code.contains(query) || name.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    @Override
    protected void onResume() {
        mAdapter.notifyDataSetChanged();
        super.onResume();
    }
}
