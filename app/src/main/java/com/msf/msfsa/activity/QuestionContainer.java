package com.msf.msfsa.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.msf.msfsa.R;
import com.msf.msfsa.application.MyApplication;
import com.msf.msfsa.events.GetAnswer;
import com.msf.msfsa.events.PostAnswer;
import com.msf.msfsa.fragment.BooleanQuestionFragment;
import com.msf.msfsa.fragment.FreeTextQuestionFragment;
import com.msf.msfsa.fragment.NumericQuestionFragment;
import com.msf.msfsa.fragment.OptionsQuestionFragment;
import com.msf.msfsa.models.Answer;
import com.msf.msfsa.models.Questions;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;

/**
 * Created by Kamil on 4/24/2016.
 */
public class QuestionContainer extends AppCompatActivity {
    int activity_id;
    long questionid;
    Questions questions;
    String client_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_container_layout);
        activity_id = getIntent().getIntExtra("activity_id", 0);
        client_id = getIntent().getStringExtra("client_id");
        questions = getIntent().getParcelableExtra("question");
//        questions = Questions.findById(Questions.class, questionid);

        selectQuestionFragment();
    }

    void selectQuestionFragment() {
        Fragment fragment = null;
//        Class questionclass;
        Log.d("qtntype before switch", " " + questions.getQuestion_Type());
        switch (questions.getQuestion_Type()) {
            case 0://free text
                fragment = FreeTextQuestionFragment.newInstance(questions);
                Log.d("fragment", fragment.getClass().getSimpleName());
                break;
            case 1://numeric
                fragment = NumericQuestionFragment.newInstance(questions);
                Log.d("fragment", fragment.getClass().getSimpleName());
                break;
            case 2://Boolean
                fragment = BooleanQuestionFragment.newInstance(questions);
                Log.d("fragment", fragment.getClass().getSimpleName());
                break;
            case 3://Options
                fragment = OptionsQuestionFragment.newInstance(questions);
                Log.d("fragment", fragment.getClass().getSimpleName());
                break;
        }
//        try {
//            fragment = (Fragment) questionclass.newInstance(questions);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.question_frame_container, fragment).commit();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void onSave(View v) {
        EventBus.getDefault().post(new GetAnswer());
    }

    @Subscribe
    public void onPostAnswer(PostAnswer postAnswer) {

        Toast.makeText(getApplicationContext(), postAnswer.getAnswer() + activity_id + "  " + client_id, Toast.LENGTH_SHORT).show();

        Answer answer = (Answer) Select.from(Answer.class)
                .where(Condition.prop("activityid").eq(activity_id),
                        Condition.prop("clientcode").eq(client_id),
                        Condition.prop("questionid").eq(questions.getQuestion_ID())).first();
        if (answer == null) {
            Calendar calendar = Calendar.getInstance();
            answer = new Answer();
            answer.setUser_Code(MyApplication.getInstance().getPrefManager().getUserid());
            answer.setOrg_ID(1);
            answer.setActivity_ID(activity_id);
            answer.setSubject_Code(1);
            answer.setQuestion_ID(questions.getQuestion_ID());
            answer.setAnswer(postAnswer.getAnswer());
            answer.setClient_Code(Integer.valueOf(client_id));
            answer.setAnswer_Date("/" + "Date(" + String.valueOf(calendar.getTimeInMillis()) + ")" + "/");
            answer.setJourney_Code(MyApplication.getInstance().generateJourneyCode());
            answer.setVisit_ID(MyApplication.getInstance().generateVisitID());
            answer.setIs_Processed(0);
            answer.setStamp_Date(answer.getAnswer_Date());
            answer.setSyncWithERP(0);
            answer.save();
            answer.setLine_ID(answer.getId());
        } else {
            answer.setAnswer(postAnswer.getAnswer());
            answer.save();
        }
    }

}
