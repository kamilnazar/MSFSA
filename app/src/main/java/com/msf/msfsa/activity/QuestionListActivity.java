package com.msf.msfsa.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.msf.msfsa.R;
import com.msf.msfsa.adapter.QuestionListAdapter;
import com.msf.msfsa.models.Questions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 4/19/2016.
 */
public class QuestionListActivity extends AppCompatActivity {
    List<Questions> questionsList = new ArrayList<>();
    RecyclerView mRecyclerView;
    QuestionListAdapter mAdapter;
    Toolbar toolbar;

    int activity_id;
    String client_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_list_activity);
        mRecyclerView = (RecyclerView) findViewById(R.id.question_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        activity_id = getIntent().getIntExtra("activity_id", 0);
        client_id = getIntent().getStringExtra("client_id");

        questionsList = Questions.find(Questions.class, "activityid = ?", String.valueOf(activity_id));
        mAdapter = new QuestionListAdapter(this, questionsList, client_id, activity_id);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        mAdapter.notifyDataSetChanged();
        super.onResume();
    }
}
