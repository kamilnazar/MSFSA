package com.msf.msfsa.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.msf.msfsa.R;
import com.msf.msfsa.application.MyApplication;
import com.msf.msfsa.events.DowloadComplete;
import com.msf.msfsa.events.DownloadFailed;
import com.msf.msfsa.events.ProgressBarStatus;
import com.msf.msfsa.events.SaveComplete;
import com.msf.msfsa.events.StartSave;
import com.msf.msfsa.models.Activity;
import com.msf.msfsa.models.ActivityAssigments;
import com.msf.msfsa.models.Answer;
import com.msf.msfsa.models.Clients;
import com.msf.msfsa.models.Groups;
import com.msf.msfsa.models.Options;
import com.msf.msfsa.models.Questions;
import com.msf.msfsa.models.Subjects;
import com.msf.msfsa.models.Users;
import com.msf.msfsa.models.gson.GetActivity;
import com.msf.msfsa.models.gson.GetActivityAssigments;
import com.msf.msfsa.models.gson.GetClients;
import com.msf.msfsa.models.gson.GetGroups;
import com.msf.msfsa.models.gson.GetOptions;
import com.msf.msfsa.models.gson.GetQuestion;
import com.msf.msfsa.models.gson.GetSubjects;
import com.msf.msfsa.models.gson.GetUsers;
import com.msf.msfsa.models.gson.PostAnswers;
import com.msf.msfsa.network.EndPoints;
import com.msf.msfsa.services.AlarmReceiver;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    static int count_ = 0;
    ProgressBar downloadProgress;
    RelativeLayout sync, syncanswer;
    TextView count;
    ProgressDialog progress;
    GetUsers userslist;
    GetActivity getActivity;
    GetActivityAssigments getActivityAssigments;
    GetClients getClients;
    GetGroups getGroups;
    GetQuestion getQuestion;
    GetSubjects getSubjects;
    GetOptions getOptions;
    int progress_size = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progress = new ProgressDialog(this);
        downloadProgress = (ProgressBar) findViewById(R.id.download_progress);
        downloadProgress.setVisibility(View.INVISIBLE);
        sync = (RelativeLayout) findViewById(R.id.syncLayout);
        syncanswer = (RelativeLayout) findViewById(R.id.syncanswer);
        count = (TextView) findViewById(R.id.count);

        setAlarm();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void makeNetworkCall(View view) {
//        sync.setClickable(false);
        count_ = 0;
//        downloadProgress.setIndeterminate(true);
//        downloadProgress.setVisibility(View.VISIBLE);

        progress.setCancelable(false);
        progress.setIndeterminate(false);
        progress.show();

        Snackbar.make(view, "Downloading data", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();

        JsonObjectRequest user = new JsonObjectRequest(EndPoints.USERS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                userslist = gson.fromJson(response.toString(), GetUsers.class);

//                downloadProgress.setIndeterminate(false);
//                downloadProgress.setMax(userslist.getGetUsersResult().size() - 1);
//                downloadProgress.setProgress(0);
//                downloadProgress.setVisibility(View.VISIBLE);

                EventBus.getDefault().post(new DowloadComplete());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e("login", "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new DownloadFailed());
                MyApplication.getInstance().cancelPendingRequests(this);

            }
        });


        JsonObjectRequest activity = new JsonObjectRequest(EndPoints.ACTIVITY, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("get user:", response.toString());
                Gson gson = new Gson();
                getActivity = gson.fromJson(response.toString(), GetActivity.class);

//                downloadProgress.setIndeterminate(false);
//                downloadProgress.setMax(getActivity.getGetAct_activityResult().size() - 1);
//                downloadProgress.setProgress(0);
//                downloadProgress.setVisibility(View.VISIBLE);

                EventBus.getDefault().post(new DowloadComplete());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e("login", "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new DownloadFailed());
                MyApplication.getInstance().cancelPendingRequests(this);

            }
        });


        JsonObjectRequest activity_assigments = new JsonObjectRequest(EndPoints.ACTIVITY_ASSIGMENTS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("get assigments:", response.toString());
                Gson gson = new Gson();
                getActivityAssigments = gson.fromJson(response.toString(), GetActivityAssigments.class);

//                downloadProgress.setIndeterminate(false);
//                downloadProgress.setMax(getActivityAssigments.getActivityAssigmentsList().size() - 1);
//                downloadProgress.setProgress(0);
//                downloadProgress.setVisibility(View.VISIBLE);

                EventBus.getDefault().post(new DowloadComplete());

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e("login", "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new DownloadFailed());
                MyApplication.getInstance().cancelPendingRequests(this);

            }
        });

        JsonObjectRequest clients = new JsonObjectRequest(EndPoints.CLIENTS + MyApplication.getInstance().getPrefManager().getUserid(), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("get clients:", response.toString());
                Gson gson = new Gson();
                getClients = gson.fromJson(response.toString(), GetClients.class);

//                Gson print = new GsonBuilder().setPrettyPrinting().create();
//                downloadProgress.setIndeterminate(false);
//                downloadProgress.setMax(getClients.getClientsList().size() - 1);
//                downloadProgress.setProgress(0);
//                downloadProgress.setVisibility(View.VISIBLE);

                EventBus.getDefault().post(new DowloadComplete());

//                Log.e("activity assig list:", print.toJson(getClients));
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                sync.setClickable(true);
                NetworkResponse networkResponse = error.networkResponse;
                Log.e("login", "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new DownloadFailed());
                MyApplication.getInstance().cancelPendingRequests(this);

            }
        });

        JsonObjectRequest groups = new JsonObjectRequest(EndPoints.GROUPS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("get clients:", response.toString());
                Gson gson = new Gson();
                getGroups = gson.fromJson(response.toString(), GetGroups.class);

//                Gson print = new GsonBuilder().setPrettyPrinting().create();
//                getGroups.save();
//                Log.e("groups:", print.toJson(getGroups));
//                downloadProgress.setIndeterminate(false);
//                downloadProgress.setMax(getGroups.getGroupsList().size() - 1);
//                downloadProgress.setProgress(0);
//                downloadProgress.setVisibility(View.VISIBLE);

                EventBus.getDefault().post(new DowloadComplete());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e("login", "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new DownloadFailed());
            }
        });

        JsonObjectRequest question = new JsonObjectRequest(EndPoints.QUESTIONS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("get clients:", response.toString());
                Gson gson = new Gson();
                getQuestion = gson.fromJson(response.toString(), GetQuestion.class);

//                Gson print = new GsonBuilder().setPrettyPrinting().create();
//                getQuestion.save();
//                Log.e("groups:", print.toJson(getQuestion));
//                downloadProgress.setIndeterminate(false);
//                downloadProgress.setMax(getQuestion.getQuestionsList().size() - 1);
//                downloadProgress.setProgress(0);
//                downloadProgress.setVisibility(View.VISIBLE);

                EventBus.getDefault().post(new DowloadComplete());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e("login", "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new DownloadFailed());
                MyApplication.getInstance().cancelPendingRequests(this);

            }
        });

        JsonObjectRequest subject = new JsonObjectRequest(EndPoints.SUBJECTS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("get clients:", response.toString());
                Gson gson = new Gson();
                getSubjects = gson.fromJson(response.toString(), GetSubjects.class);

//                Gson print = new GsonBuilder().setPrettyPrinting().create();
//                getSubjects.save();
//                Log.e("groups:", print.toJson(getSubjects));
//                downloadProgress.setIndeterminate(false);
//                downloadProgress.setMax(getSubjects.getList().size() - 1);
//                downloadProgress.setProgress(0);
//                downloadProgress.setVisibility(View.VISIBLE);

                EventBus.getDefault().post(new DowloadComplete());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e("login", "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new DownloadFailed());
                MyApplication.getInstance().cancelPendingRequests(this);

            }
        });

        JsonObjectRequest options = new JsonObjectRequest(EndPoints.OPTIONS, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("get options:", response.toString());
                Gson gson = new Gson();
                getOptions = gson.fromJson(response.toString(), GetOptions.class);

//                Gson print = new GsonBuilder().setPrettyPrinting().create();
//                getQuestion.save();
//                Log.e("groups:", print.toJson(getQuestion));
//                downloadProgress.setIndeterminate(false);
//                downloadProgress.setMax(getOptions.getGetAct_OptionsResult().size() - 1);
//                downloadProgress.setProgress(0);
//                downloadProgress.setVisibility(View.VISIBLE);

                EventBus.getDefault().post(new DowloadComplete());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e("login", "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(new DownloadFailed());
                MyApplication.getInstance().cancelPendingRequests(this);
            }
        });

        MyApplication.getInstance().addToRequestQueue(user.setTag(this));
        MyApplication.getInstance().addToRequestQueue(activity.setTag(this));
        MyApplication.getInstance().addToRequestQueue(activity_assigments.setTag(this));
        MyApplication.getInstance().addToRequestQueue(clients.setTag(this));
        MyApplication.getInstance().addToRequestQueue(groups.setTag(this));
        MyApplication.getInstance().addToRequestQueue(question.setTag(this));
        MyApplication.getInstance().addToRequestQueue(subject.setTag(this));
        MyApplication.getInstance().addToRequestQueue(options.setTag(this));
    }

    public void onJourney(View view) {
        Intent intent = new Intent(this, GroupSelectActivity.class);
        startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    public void onActivityDownloaded(GetActivity getActivity) {
//        Activity.deleteAll(Activity.class);
//        getActivity.save();
////        downloadProgress.setIndeterminate(true);
//    }
//
//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    public void onActivityAssigmentsDownloaded(GetActivityAssigments getActivityAssigments) {
//        ActivityAssigments.deleteAll(ActivityAssigments.class);
//        getActivityAssigments.save();
////        downloadProgress.setIndeterminate(true);
//    }
//
//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    public void onClientDownloaded(GetClients getClients) {
//        Clients.deleteAll(Clients.class);
//        getClients.save();
////        downloadProgress.setIndeterminate(true);
//    }
//
//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    public void onGroupDownloaded(GetGroups getGroups) {
//        Groups.deleteAll(Groups.class);
//        getGroups.save();
////        downloadProgress.setIndeterminate(true);
//    }
//
//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    public void onQuestionsDownloaded(GetQuestion getQuestion) {
//        Questions.deleteAll(Questions.class);
//        getQuestion.save();
////        downloadProgress.setIndeterminate(true);
//    }
//
//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    public void onSubjectsDownloaded(GetSubjects getSubjects) {
//        Subjects.deleteAll(Subjects.class);
//        getSubjects.save();
////        downloadProgress.setIndeterminate(true);
//    }
//
//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    public void onUserDownloaded(GetUsers getUsers) {
//        Users.deleteAll(Users.class);
//        getUsers.save();
////        downloadProgress.setIndeterminate(true);
//    }
//
//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    public void onOptionsDownloaded(GetOptions getOptions) {
//        Options.deleteAll(Options.class);
//        getOptions.save();
////        downloadProgress.setIndeterminate(true);
//    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateProgressBar(ProgressBarStatus status) {
//        downloadProgress.setProgress(status.status);
        Log.d("updateProgressBar", "called");
        progress_size++;
        progress.setProgress(progress_size);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onSaveComplete(SaveComplete saveComplete) {
//        downloadProgress.setIndeterminate(false);
        Log.d("onSaveComplete", "complete");
    }

    public void signout(View view) {
        Answer.deleteAll(Answer.class);
        MyApplication.getInstance().getPrefManager().setUserstatus(false);
        MyApplication.getInstance().getPrefManager().setUserId(0);
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        int count_ = Answer.listAll(Answer.class).size();
        count.setText(String.valueOf(count_));
    }

    public void syncanswer(View view) {
        progress.show();
        syncanswer.setClickable(false);
        JSONObject postdata = new JSONObject();
        try {
            postdata.put("User_Code", String.valueOf(MyApplication.getInstance().getPrefManager().getUserid()));
        } catch (JSONException e) {
        }
        final JsonObjectRequest saveAns = new JsonObjectRequest(Request.Method.POST, EndPoints.SAVE_ANS, postdata,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        syncanswer.setClickable(true);
                        progress.hide();
                        try {
                            if (response.getBoolean("SaveDataResult")) {
                                Answer.deleteAll(Answer.class);
                                count.setText("0");
                                Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("volley", error.getMessage());
                syncanswer.setClickable(true);
                progress.hide();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
//                headers.put("Authorization", "token " + user.getAcces_token());
                return headers;
            }
        };

        Gson ansjson = new Gson();
        PostAnswers postAnswers = new PostAnswers();
        postAnswers.setSetAct_AnswersResult(Answer.listAll(Answer.class));
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(ansjson.toJson(postAnswers));
            jsonObject.put("User_Code", MyApplication.getInstance().getPrefManager().getUserid());
            Log.e("groups:", jsonObject.toString());
        } catch (JSONException e) {
        }
        JsonObjectRequest postans = new JsonObjectRequest(Request.Method.POST, EndPoints.ANSWERS, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getBoolean("SetAct_AnswersResult")) {
                        MyApplication.getInstance().addToRequestQueue(saveAns);
                    }
                } catch (JSONException e) {
                }
                syncanswer.setClickable(true);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.e("volley", error.getMessage());
                syncanswer.setClickable(true);
                progress.hide();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
//                headers.put("Authorization", "token " + user.getAcces_token());
                return headers;
            }

        };

        MyApplication.getInstance().addToRequestQueue(postans);
//        PostAnswers postAnswers = new PostAnswers();
//        postAnswers.setSetAct_AnswersResult(Answer.listAll(Answer.class));
//        Gson print = new GsonBuilder().setPrettyPrinting().create();
//        Log.e("answer:", print.toJson(postAnswers));
    }

    public void showProgressDialog() {
        progress = ProgressDialog.show(this, "Downloading", "", true);
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onDownloadComplete(DowloadComplete dowloadComplete) {
        count_++;
        if (count_ == 8) {
            EventBus.getDefault().post(new StartSave());
        }
    }

    @Subscribe
    public void onDownloadFailed(DownloadFailed downloadFailed) {
        progress.hide();
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onStartStave(StartSave startSave) {
        progress_size = 0;
        int size = userslist.getSize()
                + getActivity.getSize()
                + getActivityAssigments.getSize()
                + getClients.getSize()
                + getGroups.getSize()
                + getOptions.getSize()
                + getQuestion.getSize()
                + getSubjects.getSize();

        progress.setIndeterminate(true);

        Activity.deleteAll(Activity.class);
        ActivityAssigments.deleteAll(ActivityAssigments.class);
        Clients.deleteAll(Clients.class);
        Groups.deleteAll(Groups.class);
        Options.deleteAll(Options.class);
        Questions.deleteAll(Questions.class);
        Subjects.deleteAll(Subjects.class);
        Users.deleteAll(Users.class);

        progress.setIndeterminate(false);
        progress.setMax(size);

        getActivity.save();
        getActivityAssigments.save();
        getClients.save();
        getGroups.save();
        getOptions.save();
        getQuestion.save();
        getSubjects.save();
        userslist.save();

        progress.hide();
    }

    void setAlarm() {
        PendingIntent pendingIntent;
        AlarmManager manager;

        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);

        manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        int interval = 60000;

        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        Toast.makeText(this, "Alarm Set", Toast.LENGTH_SHORT).show();
    }

}
