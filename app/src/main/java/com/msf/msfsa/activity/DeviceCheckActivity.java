package com.msf.msfsa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.msf.msfsa.R;
import com.msf.msfsa.application.MyApplication;
import com.msf.msfsa.network.EndPoints;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Kamil on 5/9/2016.
 */
public class DeviceCheckActivity extends AppCompatActivity {
    ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_check_layout);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setIndeterminate(true);
        final String url = EndPoints.IS_ACTIVE + MyApplication.getInstance().getWifiMacAddress();
        if (true) {
            Intent intent = new Intent(DeviceCheckActivity.this, LoginActivity.class);
            startActivity(intent);
        } else {
//            JsonObjectRequest isactive = new JsonObjectRequest(url, null,
//                    new Response.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            try {
//                                Log.d("response", response.toString());
//                                boolean result = response.getJSONArray("GetActive_DeviceResult").getJSONObject(0).getBoolean("isActive");
//                                MyApplication.getInstance().getPrefManager().setIsActive(result);
//                                if (result) {
//                                    Intent intent = new Intent(DeviceCheckActivity.this, LoginActivity.class);
//                                    startActivity(intent);
//                                } else {
//                                    Toast.makeText(getApplicationContext(), "Unexpectedly closed", Toast.LENGTH_SHORT).show();
//                                    finish();
//                                }
//                            } catch (JSONException e) {
//
//                            }
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            Toast.makeText(getApplicationContext(), "Not Authorized", Toast.LENGTH_SHORT).show();
//                        }
//                    });
//            MyApplication.getInstance().addToRequestQueue(isactive);
            AsyncHttpClient client = new AsyncHttpClient();
            client.get(url, new JsonHttpResponseHandler() {

                @Override
                public void onStart() {
                    // called before request is started
                    Log.d("url", url);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    // If the response is JSONObject instead of expected JSONArray
                    try {
                        Log.d("response", response.toString());
                        boolean result = response.getJSONArray("GetActive_DeviceResult").getJSONObject(0).getBoolean("isActive");
                        MyApplication.getInstance().getPrefManager().setIsActive(result);
                        if (result) {
                            Intent intent = new Intent(DeviceCheckActivity.this, LoginActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "Unexpectedly closed", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    } catch (JSONException e) {

                    }
                }

                @Override
                public void onRetry(int retryNo) {
                    // called when request is retried
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
