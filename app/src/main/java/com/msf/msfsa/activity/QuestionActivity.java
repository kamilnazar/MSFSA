package com.msf.msfsa.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.msf.msfsa.fragment.BooleanQuestionFragment;
import com.msf.msfsa.fragment.FreeTextQuestionFragment;
import com.msf.msfsa.fragment.NumericQuestionFragment;
import com.msf.msfsa.fragment.OptionsQuestionFragment;
import com.msf.msfsa.models.Questions;

import java.util.ArrayList;
import java.util.List;

import ivb.com.materialstepper.simpleMobileStepper;

/**
 * Created by Kamil on 4/8/2016.
 */
public class QuestionActivity extends simpleMobileStepper {
    List<Fragment> stepperFragmentList = new ArrayList<>();
    int client_id, activity_id;
    List<Questions> questions;

    @Override
    public List<Fragment> init() {
        for (Questions question : questions) {
            switch (question.getQuestion_Type()) {
                case 0://free text
                    stepperFragmentList.add(FreeTextQuestionFragment.newInstance(question));
                    break;
                case 1://numeric
                    stepperFragmentList.add(NumericQuestionFragment.newInstance(question));
                    break;
                case 2://Boolean
                    stepperFragmentList.add(BooleanQuestionFragment.newInstance(question));
                    break;
                case 3://Options
                    stepperFragmentList.add(OptionsQuestionFragment.newInstance(question));
                    break;
            }
        }
        return stepperFragmentList;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        activity_id = getIntent().getIntExtra("activity_id", 0);
        client_id = getIntent().getIntExtra("client_id", 0);
        questions = Questions.find(Questions.class, "activityid = ?", String.valueOf(activity_id));
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStepperCompleted() {
        showCompletedDialog();
    }

    protected void showCompletedDialog() {
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(
                QuestionActivity.this);

        // set title
        alertDialogBuilder.setTitle("Alert");
        alertDialogBuilder
                .setMessage("Survey completed")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });

        // create alert dialog
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }
}
