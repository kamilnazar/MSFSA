package com.msf.msfsa.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.msf.msfsa.R;
import com.msf.msfsa.events.GetAnswer;
import com.msf.msfsa.events.PostAnswer;
import com.msf.msfsa.models.Questions;
import com.vi.swipenumberpicker.OnValueChangeListener;
import com.vi.swipenumberpicker.SwipeNumberPicker;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Kamil on 4/8/2016.
 */
public class NumericQuestionFragment extends Fragment {
    Questions question;
    SwipeNumberPicker numberPicker;

    public static NumericQuestionFragment newInstance(Questions questions) {
        NumericQuestionFragment fragment = new NumericQuestionFragment();
        Bundle args = new Bundle();
        args.putParcelable("question", questions);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.numeric_question_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        question = getArguments().getParcelable("question");
        numberPicker = (SwipeNumberPicker) view.findViewById(R.id.num_select1);
        numberPicker.setMinValue(question.getMin_Value());
        numberPicker.setMaxValue(question.getMax_Value());
//        numberPicker.setWrapSelectorWheel(true);
//        numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        numberPicker.setOnValueChangeListener(new OnValueChangeListener() {
            @Override
            public boolean onValueChange(SwipeNumberPicker swipeNumberPicker, int i, int i1) {
                return true;
            }
        });
        TextView question_text = (TextView) view.findViewById(R.id.question_text);
        question_text.setText(question.getQuestion_Description());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onGetAnswer(GetAnswer getAnswer) {
        PostAnswer postAnswer = new PostAnswer();
        postAnswer.setAnswer(String.valueOf(numberPicker.getValue()));
        EventBus.getDefault().post(postAnswer);
    }
}
