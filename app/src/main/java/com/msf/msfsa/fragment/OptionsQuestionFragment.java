package com.msf.msfsa.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.msf.msfsa.R;
import com.msf.msfsa.adapter.OptionListAdapter;
import com.msf.msfsa.events.GetAnswer;
import com.msf.msfsa.events.PostAnswer;
import com.msf.msfsa.models.Options;
import com.msf.msfsa.models.Questions;
import com.orm.query.Condition;
import com.orm.query.Select;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 4/8/2016.
 */
public class OptionsQuestionFragment extends Fragment {
    ListView listView;
    Questions question;
    List<Options> options = new ArrayList<>();

    public static OptionsQuestionFragment newInstance(Questions questions) {
        OptionsQuestionFragment fragment = new OptionsQuestionFragment();
        Bundle args = new Bundle();
        args.putParcelable("question", questions);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.options_question_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        question = getArguments().getParcelable("question");
        listView = (ListView) view.findViewById(R.id.options_list);
        TextView question_text = (TextView) view.findViewById(R.id.question_text);
        options = Select.from(Options.class).where(Condition.prop("questionid").eq(question.getQuestion_ID())).list();
        OptionListAdapter adapter = new OptionListAdapter(getActivity(), options);
        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setSelector(R.drawable.selector);

        question_text.setText(question.getQuestion_Description());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onGetAnswer(GetAnswer getAnswer) {
        int choiceid = listView.getSelectedItemPosition();
        PostAnswer postAnswer = new PostAnswer(options.get(choiceid).getOption_Description());
        EventBus.getDefault().post(postAnswer);
    }
}
