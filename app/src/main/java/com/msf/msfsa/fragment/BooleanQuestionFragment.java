package com.msf.msfsa.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.msf.msfsa.R;
import com.msf.msfsa.events.GetAnswer;
import com.msf.msfsa.events.PostAnswer;
import com.msf.msfsa.models.Questions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import info.hoang8f.android.segmented.SegmentedGroup;

/**
 * Created by Kamil on 4/8/2016.
 */
public class BooleanQuestionFragment extends Fragment {
    Questions question;
    SegmentedGroup segmentedGroup;
    PostAnswer postAnswer = new PostAnswer();

    public static BooleanQuestionFragment newInstance(Questions questions) {
        BooleanQuestionFragment fragment = new BooleanQuestionFragment();
        Bundle args = new Bundle();
        args.putParcelable("question", questions);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.boolean_question_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        question = getArguments().getParcelable("question");
        TextView question_text = (TextView) view.findViewById(R.id.question_text);
        question_text.setText(question.getQuestion_Description());

        segmentedGroup = (SegmentedGroup) view.findViewById(R.id.boolean_ans);
        segmentedGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.yes:
                        postAnswer.setAnswer("yes");
//                        Toast.makeText(getActivity(), "One", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.no:
                        postAnswer.setAnswer("no");
//                        Toast.makeText(getActivity(), "Two", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onGetAnswer(GetAnswer getAnswer) {
        EventBus.getDefault().post(postAnswer);
    }
}
