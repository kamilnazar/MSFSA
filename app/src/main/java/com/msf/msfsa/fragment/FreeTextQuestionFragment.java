package com.msf.msfsa.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.msf.msfsa.R;
import com.msf.msfsa.events.GetAnswer;
import com.msf.msfsa.events.PostAnswer;
import com.msf.msfsa.models.Questions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Kamil on 4/8/2016.
 */
public class FreeTextQuestionFragment extends Fragment {
    Questions question;
    PostAnswer postAnswer = new PostAnswer();
    TextView answer;

    public static FreeTextQuestionFragment newInstance(Questions questions) {
        FreeTextQuestionFragment fragment = new FreeTextQuestionFragment();
        Bundle args = new Bundle();
        args.putParcelable("question", questions);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.freetext_question_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        question = getArguments().getParcelable("question");
        TextView question_text = (TextView) view.findViewById(R.id.question_text);
        answer = (TextView) view.findViewById(R.id.freetext_ans);
        question_text.setText(question.getQuestion_Description());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onGetAnswer(GetAnswer getAnswer) {
        if (answer.getText().length() != 0) {
            postAnswer.setAnswer(answer.getText().toString());
            EventBus.getDefault().post(postAnswer);
        } else {
            Toast.makeText(getActivity(), "Please enter a valid entry", Toast.LENGTH_SHORT).show();
        }
    }

}
