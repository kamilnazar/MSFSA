package com.msf.msfsa.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.msfsa.R;
import com.msf.msfsa.adapter.ActivityListAdapter;
import com.msf.msfsa.models.Activity;
import com.orm.query.Condition;
import com.orm.query.Select;

/**
 * Created by Kamil on 4/8/2016.
 */
public class ActivityListFragment extends Fragment {
    ActivityListAdapter adapter;

    public static ActivityListFragment newInstance(int id) {
        ActivityListFragment fragment = new ActivityListFragment();
        Bundle arg = new Bundle();
        arg.putInt("group_id", id);
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new ActivityListAdapter(getActivity(),
                Select.from(Activity.class)
                        .where(Condition.prop("activityid").like(String.valueOf(getArguments().getInt("group_id")) + "%")).list()
        );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Defines the xml file for the fragment
        return inflater.inflate(R.layout.activity_fragment_layout, parent, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.activity_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
    }
}
