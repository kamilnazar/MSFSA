package com.msf.msfsa.events;

/**
 * Created by Kamil on 4/24/2016.
 */
public class PostAnswer {
    String answer;

    public PostAnswer() {
    }

    public PostAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
