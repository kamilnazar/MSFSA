package com.msf.msfsa.events;

/**
 * Created by Kamil on 4/7/2016.
 */
public class ProgressBarStatus {
    public final int status;

    public ProgressBarStatus(int status) {
        this.status = status;
    }
}
