package com.msf.msfsa.network;

/**
 * Created by Kamil on 3/25/2016.
 */
public class EndPoints {
    public static String BASE_URL = "http://212.107.99.254/msf/Service1.svc";
    public static String ACTIVITY = BASE_URL + "/GetActivities";
    public static String QUESTIONS = BASE_URL + "/GetAct_questions";
    public static String CLIENTS = BASE_URL + "/GetClients?user_code=";
    public static String SUBJECTS = BASE_URL + "/GetAct_Subjects";
    public static String USERS = BASE_URL + "/GetUsers?org_id=1";
    public static String ACTIVITY_ASSIGMENTS = BASE_URL + "/GetActivity_assignments";
    public static String GROUPS = BASE_URL + "/GetAct_Groups";
    public static String OPTIONS = BASE_URL + "/GetAct_options";
    public static String ANSWERS = BASE_URL + "/SetAct_Answers";
    public static String SAVE_ANS = BASE_URL + "/SaveData";
    public static String IS_ACTIVE = BASE_URL + "/GetActive_Device?Device_Id=";
    public static String SET_VISIT=BASE_URL+"/SetVisit";
}
