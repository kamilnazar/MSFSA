package com.msf.msfsa.models.gson;

import com.msf.msfsa.events.ProgressBarStatus;
import com.msf.msfsa.events.SaveComplete;
import com.msf.msfsa.models.Users;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 3/26/2016.
 */
public class GetUsers {
    private List<Users> GetUsersResult = new ArrayList<Users>();

    public GetUsers(List<Users> getUsersResult) {
        GetUsersResult = getUsersResult;
    }

    public GetUsers() {
    }

    public List<Users> getGetUsersResult() {
        return GetUsersResult;
    }

    public void setGetUsersResult(List<Users> getUsersResult) {
        GetUsersResult = getUsersResult;
    }

    public void save() {
        for (int i = 0; i < this.GetUsersResult.size(); i++) {
            this.GetUsersResult.get(i).save();
            EventBus.getDefault().post(new ProgressBarStatus(i));

        }
        EventBus.getDefault().post(new SaveComplete());
    }

    public int getSize() {
        return GetUsersResult.size();
    }
}
