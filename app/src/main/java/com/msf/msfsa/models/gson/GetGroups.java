package com.msf.msfsa.models.gson;

import com.msf.msfsa.events.ProgressBarStatus;
import com.msf.msfsa.events.SaveComplete;
import com.msf.msfsa.models.Groups;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 3/26/2016.
 */
public class GetGroups {
    private List<Groups> GetAct_GroupsResult = new ArrayList<>();

    public GetGroups() {
    }

    public GetGroups(List<Groups> groupsList) {
        this.GetAct_GroupsResult = groupsList;
    }

    public List<Groups> getGroupsList() {
        return GetAct_GroupsResult;
    }

    public void setGroupsList(List<Groups> groupsList) {
        this.GetAct_GroupsResult = groupsList;
    }

    public void save() {
        for (int i = 0; i < this.GetAct_GroupsResult.size(); i++) {
            this.GetAct_GroupsResult.get(i).save();
            EventBus.getDefault().post(new ProgressBarStatus(i));

        }
        EventBus.getDefault().post(new SaveComplete());
    }

    public int getSize() {
        return GetAct_GroupsResult.size();
    }
}
