package com.msf.msfsa.models.gson;

import com.msf.msfsa.models.Visits;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 5/15/2016.
 */
public class SetVisit {
    List <Visits> Visits=new ArrayList<>();

    public SetVisit(List<com.msf.msfsa.models.Visits> visits) {
        Visits = visits;
    }

    public List<Visits> getVisits() {
        return Visits;
    }

    public void setVisits(List<Visits> visits) {
        this.Visits = visits;
    }
}
