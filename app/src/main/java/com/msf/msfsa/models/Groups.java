package com.msf.msfsa.models;

import com.orm.SugarRecord;

/**
 * Created by Kamil on 3/25/2016.
 */
public class Groups extends SugarRecord {


    /**
     * Act_Group_Alt_Description : General
     * Act_Group_Code : 10
     * Act_Group_Description : General
     * Desc : null
     * IsActive : true
     * Status : null
     * group_ID : 0
     */

    private String Act_Group_Alt_Description;
    private int Act_Group_Code;
    private String Act_Group_Description;
    private Object Desc;
    private boolean IsActive;
    private Object Status;
    private int group_ID;

    public String getAct_Group_Alt_Description() {
        return Act_Group_Alt_Description;
    }

    public void setAct_Group_Alt_Description(String Act_Group_Alt_Description) {
        this.Act_Group_Alt_Description = Act_Group_Alt_Description;
    }

    public int getAct_Group_Code() {
        return Act_Group_Code;
    }

    public void setAct_Group_Code(int Act_Group_Code) {
        this.Act_Group_Code = Act_Group_Code;
    }

    public String getAct_Group_Description() {
        return Act_Group_Description;
    }

    public void setAct_Group_Description(String Act_Group_Description) {
        this.Act_Group_Description = Act_Group_Description;
    }

    public Object getDesc() {
        return Desc;
    }

    public void setDesc(Object Desc) {
        this.Desc = Desc;
    }

    public boolean isIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean IsActive) {
        this.IsActive = IsActive;
    }

    public Object getStatus() {
        return Status;
    }

    public void setStatus(Object Status) {
        this.Status = Status;
    }

    public int getGroup_ID() {
        return group_ID;
    }

    public void setGroup_ID(int group_ID) {
        this.group_ID = group_ID;
    }
}
