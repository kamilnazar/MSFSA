package com.msf.msfsa.models;

import com.orm.SugarRecord;

/**
 * Created by Kamil on 3/25/2016.
 */
public class Users extends SugarRecord {

    /**
     * Balance : 0
     * Branch_Code :
     * Credit_Limit : 0
     * IsCashVan : 0
     * Is_Blocked : 0
     * Org_ID : 1
     * Parent_Code :
     * Prefix_ID : 317
     * Receipt_Voucher_Sequence : 0
     * Route_ID : null
     * Stamp_Date : /Date(1396363443863+0300)/
     * User_Alt_Description : ASIF BASHEER
     * User_Code : 0317
     * User_Description : ASIF BASHEER MOHAMMED BASHEER
     * User_Group_Code : 1
     * User_Password : 8
     * User_Phone_Number :
     * User_Type : 3
     */

    private int Balance;
    private String Branch_Code;
    private int Credit_Limit;
    private int IsCashVan;
    private int Is_Blocked;
    private String Org_ID;
    private String Parent_Code;
    private int Prefix_ID;
    private int Receipt_Voucher_Sequence;
    private Object Route_ID;
    private String Stamp_Date;
    private String User_Alt_Description;
    private String User_Code;
    private String User_Description;
    private String User_Group_Code;
    private String User_Password;
    private String User_Phone_Number;
    private int User_Type;

    public Users() {
    }

    public Users(int balance, String branch_Code, int credit_Limit, int isCashVan, int is_Blocked, String org_ID, String parent_Code, int prefix_ID, int receipt_Voucher_Sequence, Object route_ID, String stamp_Date, String user_Alt_Description, String user_Code, String user_Description, String user_Group_Code, String user_Password, String user_Phone_Number, int user_Type) {
        Balance = balance;
        Branch_Code = branch_Code;
        Credit_Limit = credit_Limit;
        IsCashVan = isCashVan;
        Is_Blocked = is_Blocked;
        Org_ID = org_ID;
        Parent_Code = parent_Code;
        Prefix_ID = prefix_ID;
        Receipt_Voucher_Sequence = receipt_Voucher_Sequence;
        Route_ID = route_ID;
        Stamp_Date = stamp_Date;
        User_Alt_Description = user_Alt_Description;
        User_Code = user_Code;
        User_Description = user_Description;
        User_Group_Code = user_Group_Code;
        User_Password = user_Password;
        User_Phone_Number = user_Phone_Number;
        User_Type = user_Type;
    }

    public int getBalance() {
        return Balance;
    }

    public void setBalance(int Balance) {
        this.Balance = Balance;
    }

    public String getBranch_Code() {
        return Branch_Code;
    }

    public void setBranch_Code(String Branch_Code) {
        this.Branch_Code = Branch_Code;
    }

    public int getCredit_Limit() {
        return Credit_Limit;
    }

    public void setCredit_Limit(int Credit_Limit) {
        this.Credit_Limit = Credit_Limit;
    }

    public int getIsCashVan() {
        return IsCashVan;
    }

    public void setIsCashVan(int IsCashVan) {
        this.IsCashVan = IsCashVan;
    }

    public int getIs_Blocked() {
        return Is_Blocked;
    }

    public void setIs_Blocked(int Is_Blocked) {
        this.Is_Blocked = Is_Blocked;
    }

    public String getOrg_ID() {
        return Org_ID;
    }

    public void setOrg_ID(String Org_ID) {
        this.Org_ID = Org_ID;
    }

    public String getParent_Code() {
        return Parent_Code;
    }

    public void setParent_Code(String Parent_Code) {
        this.Parent_Code = Parent_Code;
    }

    public int getPrefix_ID() {
        return Prefix_ID;
    }

    public void setPrefix_ID(int Prefix_ID) {
        this.Prefix_ID = Prefix_ID;
    }

    public int getReceipt_Voucher_Sequence() {
        return Receipt_Voucher_Sequence;
    }

    public void setReceipt_Voucher_Sequence(int Receipt_Voucher_Sequence) {
        this.Receipt_Voucher_Sequence = Receipt_Voucher_Sequence;
    }

    public Object getRoute_ID() {
        return Route_ID;
    }

    public void setRoute_ID(Object Route_ID) {
        this.Route_ID = Route_ID;
    }

    public String getStamp_Date() {
        return Stamp_Date;
    }

    public void setStamp_Date(String Stamp_Date) {
        this.Stamp_Date = Stamp_Date;
    }

    public String getUser_Alt_Description() {
        return User_Alt_Description;
    }

    public void setUser_Alt_Description(String User_Alt_Description) {
        this.User_Alt_Description = User_Alt_Description;
    }

    public String getUser_Code() {
        return User_Code;
    }

    public void setUser_Code(String User_Code) {
        this.User_Code = User_Code;
    }

    public String getUser_Description() {
        return User_Description;
    }

    public void setUser_Description(String User_Description) {
        this.User_Description = User_Description;
    }

    public String getUser_Group_Code() {
        return User_Group_Code;
    }

    public void setUser_Group_Code(String User_Group_Code) {
        this.User_Group_Code = User_Group_Code;
    }

    public String getUser_Password() {
        return User_Password;
    }

    public void setUser_Password(String User_Password) {
        this.User_Password = User_Password;
    }

    public String getUser_Phone_Number() {
        return User_Phone_Number;
    }

    public void setUser_Phone_Number(String User_Phone_Number) {
        this.User_Phone_Number = User_Phone_Number;
    }

    public int getUser_Type() {
        return User_Type;
    }

    public void setUser_Type(int User_Type) {
        this.User_Type = User_Type;
    }
}
