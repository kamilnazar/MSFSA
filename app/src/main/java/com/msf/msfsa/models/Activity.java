package com.msf.msfsa.models;

import com.orm.SugarRecord;

/**
 * Created by Kamil on 3/25/2016.
 */
public class Activity extends SugarRecord {
    /**
     * Activity_Alt_Description : admin
     * Activity_Description : admin
     * Activity_ID : 2
     * End_Date : /Date(1461618000000+0300)/
     * IsActive : true
     * Org_ID : 1
     * Stamp_Date : /Date(1395814646723+0300)/
     * Start_Date : /Date(1395781200000+0300)/
     * Subject_Type : 7
     */


    private String Activity_Alt_Description;
    private String Activity_Description;
    private int Activity_ID;
    private String End_Date;
    private boolean IsActive;
    private String Org_ID;
    private String Stamp_Date;
    private String Start_Date;
    private int Subject_Type;

    public Activity() {
    }

    public Activity(String activity_Alt_Description, String activity_Description, int activity_ID, String end_Date, boolean isActive, String org_ID, String stamp_Date, String start_Date, int subject_Type) {
        Activity_Alt_Description = activity_Alt_Description;
        Activity_Description = activity_Description;
        Activity_ID = activity_ID;
        End_Date = end_Date;
        IsActive = isActive;
        Org_ID = org_ID;
        Stamp_Date = stamp_Date;
        Start_Date = start_Date;
        Subject_Type = subject_Type;
    }

    public String getActivity_Alt_Description() {
        return Activity_Alt_Description;
    }

    public void setActivity_Alt_Description(String Activity_Alt_Description) {
        this.Activity_Alt_Description = Activity_Alt_Description;
    }

    public String getActivity_Description() {
        return Activity_Description;
    }

    public void setActivity_Description(String Activity_Description) {
        this.Activity_Description = Activity_Description;
    }

    public int getActivity_ID() {
        return Activity_ID;
    }

    public void setActivity_ID(int Activity_ID) {
        this.Activity_ID = Activity_ID;
    }

    public String getEnd_Date() {
        return End_Date;
    }

    public void setEnd_Date(String End_Date) {
        this.End_Date = End_Date;
    }

    public boolean isIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean IsActive) {
        this.IsActive = IsActive;
    }

    public String getOrg_ID() {
        return Org_ID;
    }

    public void setOrg_ID(String Org_ID) {
        this.Org_ID = Org_ID;
    }

    public String getStamp_Date() {
        return Stamp_Date;
    }

    public void setStamp_Date(String Stamp_Date) {
        this.Stamp_Date = Stamp_Date;
    }

    public String getStart_Date() {
        return Start_Date;
    }

    public void setStart_Date(String Start_Date) {
        this.Start_Date = Start_Date;
    }

    public int getSubject_Type() {
        return Subject_Type;
    }

    public void setSubject_Type(int Subject_Type) {
        this.Subject_Type = Subject_Type;
    }
}
