package com.msf.msfsa.models;

import com.orm.SugarRecord;

/**
 * Created by Kamil on 3/25/2016.
 */
public class Clients extends SugarRecord {


    /**
     * Address : ALTANEEM
     * Alt_Address : التنعيم
     * Area_Of_Business : null
     * Area_Of_Business_Code : 1
     * Barcode :
     * Branch_Plant_Code : 1
     * Census_Client_Code :
     * Channel : null
     * Channel_Code : 9
     * City : null
     * City_Code : 201
     * Client_Agings : null
     * Client_Alt_Description : بقالة دانية
     * Client_Classification : CS
     * Client_Code : 100202
     * Client_Creditings : null
     * Client_Description : DANIYA
     * Client_Status_ID : 1
     * Client_Type_ID : 0
     * Collection_Type : 0
     * Contact_Person : ABO BAKER
     * Currency_Code : SAR
     * Email :
     * Fax_Number :
     * IsFromERP : false
     * Is_Dummy : false
     * Is_From_Census : false
     * Latitude : 0
     * Longitude : 0
     * Mouhafazat_Code : 20104
     * Org_ID : 1
     * PO_Box :
     * Parent_Client_Code : 10
     * Payment_Term_Code : 2
     * Payment_Terms : null
     * Phone_Number :
     * Price_List_Code : 1
     * Qadaa_Code : 2010401
     * Region_Code : 2
     * Sq_Meter_Area :
     * Stamp_Date : /Date(1434445492250+0300)/
     * Tax_Code :
     * User_Code : 28
     * VAT_Number :
     * Year_Of_Service : null
     */

    private String Address;
    private String Alt_Address;
    private Object Area_Of_Business;
    private String Area_Of_Business_Code;
    private String Barcode;
    private String Branch_Plant_Code;
    private String Census_Client_Code;
    private Object Channel;
    private String Channel_Code;
    private Object City;
    private String City_Code;
    private Object Client_Agings;
    private String Client_Alt_Description;
    private String Client_Classification;
    private String Client_Code;
    private Object Client_Creditings;
    private String Client_Description;
    private int Client_Status_ID;
    private int Client_Type_ID;
    private int Collection_Type;
    private String Contact_Person;
    private String Currency_Code;
    private String Email;
    private String Fax_Number;
    private boolean IsFromERP;
    private boolean Is_Dummy;
    private boolean Is_From_Census;
    private double Latitude;
    private double Longitude;
    private String Mouhafazat_Code;
    private String Org_ID;
    private String PO_Box;
    private String Parent_Client_Code;
    private String Payment_Term_Code;
    private Object Payment_Terms;
    private String Phone_Number;
    private String Price_List_Code;
    private String Qadaa_Code;
    private String Region_Code;
    private String Sq_Meter_Area;
    private String Stamp_Date;
    private String Tax_Code;
    private String User_Code;
    private String VAT_Number;
    private Object Year_Of_Service;

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getAlt_Address() {
        return Alt_Address;
    }

    public void setAlt_Address(String Alt_Address) {
        this.Alt_Address = Alt_Address;
    }

    public Object getArea_Of_Business() {
        return Area_Of_Business;
    }

    public void setArea_Of_Business(Object Area_Of_Business) {
        this.Area_Of_Business = Area_Of_Business;
    }

    public String getArea_Of_Business_Code() {
        return Area_Of_Business_Code;
    }

    public void setArea_Of_Business_Code(String Area_Of_Business_Code) {
        this.Area_Of_Business_Code = Area_Of_Business_Code;
    }

    public String getBarcode() {
        return Barcode;
    }

    public void setBarcode(String Barcode) {
        this.Barcode = Barcode;
    }

    public String getBranch_Plant_Code() {
        return Branch_Plant_Code;
    }

    public void setBranch_Plant_Code(String Branch_Plant_Code) {
        this.Branch_Plant_Code = Branch_Plant_Code;
    }

    public String getCensus_Client_Code() {
        return Census_Client_Code;
    }

    public void setCensus_Client_Code(String Census_Client_Code) {
        this.Census_Client_Code = Census_Client_Code;
    }

    public Object getChannel() {
        return Channel;
    }

    public void setChannel(Object Channel) {
        this.Channel = Channel;
    }

    public String getChannel_Code() {
        return Channel_Code;
    }

    public void setChannel_Code(String Channel_Code) {
        this.Channel_Code = Channel_Code;
    }

    public Object getCity() {
        return City;
    }

    public void setCity(Object City) {
        this.City = City;
    }

    public String getCity_Code() {
        return City_Code;
    }

    public void setCity_Code(String City_Code) {
        this.City_Code = City_Code;
    }

    public Object getClient_Agings() {
        return Client_Agings;
    }

    public void setClient_Agings(Object Client_Agings) {
        this.Client_Agings = Client_Agings;
    }

    public String getClient_Alt_Description() {
        return Client_Alt_Description;
    }

    public void setClient_Alt_Description(String Client_Alt_Description) {
        this.Client_Alt_Description = Client_Alt_Description;
    }

    public String getClient_Classification() {
        return Client_Classification;
    }

    public void setClient_Classification(String Client_Classification) {
        this.Client_Classification = Client_Classification;
    }

    public String getClient_Code() {
        return Client_Code;
    }

    public void setClient_Code(String Client_Code) {
        this.Client_Code = Client_Code;
    }

    public Object getClient_Creditings() {
        return Client_Creditings;
    }

    public void setClient_Creditings(Object Client_Creditings) {
        this.Client_Creditings = Client_Creditings;
    }

    public String getClient_Description() {
        return Client_Description;
    }

    public void setClient_Description(String Client_Description) {
        this.Client_Description = Client_Description;
    }

    public int getClient_Status_ID() {
        return Client_Status_ID;
    }

    public void setClient_Status_ID(int Client_Status_ID) {
        this.Client_Status_ID = Client_Status_ID;
    }

    public int getClient_Type_ID() {
        return Client_Type_ID;
    }

    public void setClient_Type_ID(int Client_Type_ID) {
        this.Client_Type_ID = Client_Type_ID;
    }

    public int getCollection_Type() {
        return Collection_Type;
    }

    public void setCollection_Type(int Collection_Type) {
        this.Collection_Type = Collection_Type;
    }

    public String getContact_Person() {
        return Contact_Person;
    }

    public void setContact_Person(String Contact_Person) {
        this.Contact_Person = Contact_Person;
    }

    public String getCurrency_Code() {
        return Currency_Code;
    }

    public void setCurrency_Code(String Currency_Code) {
        this.Currency_Code = Currency_Code;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getFax_Number() {
        return Fax_Number;
    }

    public void setFax_Number(String Fax_Number) {
        this.Fax_Number = Fax_Number;
    }

    public boolean isIsFromERP() {
        return IsFromERP;
    }

    public void setIsFromERP(boolean IsFromERP) {
        this.IsFromERP = IsFromERP;
    }

    public boolean isIs_Dummy() {
        return Is_Dummy;
    }

    public void setIs_Dummy(boolean Is_Dummy) {
        this.Is_Dummy = Is_Dummy;
    }

    public boolean isIs_From_Census() {
        return Is_From_Census;
    }

    public void setIs_From_Census(boolean Is_From_Census) {
        this.Is_From_Census = Is_From_Census;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double Latitude) {
        this.Latitude = Latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double Longitude) {
        this.Longitude = Longitude;
    }

    public String getMouhafazat_Code() {
        return Mouhafazat_Code;
    }

    public void setMouhafazat_Code(String Mouhafazat_Code) {
        this.Mouhafazat_Code = Mouhafazat_Code;
    }

    public String getOrg_ID() {
        return Org_ID;
    }

    public void setOrg_ID(String Org_ID) {
        this.Org_ID = Org_ID;
    }

    public String getPO_Box() {
        return PO_Box;
    }

    public void setPO_Box(String PO_Box) {
        this.PO_Box = PO_Box;
    }

    public String getParent_Client_Code() {
        return Parent_Client_Code;
    }

    public void setParent_Client_Code(String Parent_Client_Code) {
        this.Parent_Client_Code = Parent_Client_Code;
    }

    public String getPayment_Term_Code() {
        return Payment_Term_Code;
    }

    public void setPayment_Term_Code(String Payment_Term_Code) {
        this.Payment_Term_Code = Payment_Term_Code;
    }

    public Object getPayment_Terms() {
        return Payment_Terms;
    }

    public void setPayment_Terms(Object Payment_Terms) {
        this.Payment_Terms = Payment_Terms;
    }

    public String getPhone_Number() {
        return Phone_Number;
    }

    public void setPhone_Number(String Phone_Number) {
        this.Phone_Number = Phone_Number;
    }

    public String getPrice_List_Code() {
        return Price_List_Code;
    }

    public void setPrice_List_Code(String Price_List_Code) {
        this.Price_List_Code = Price_List_Code;
    }

    public String getQadaa_Code() {
        return Qadaa_Code;
    }

    public void setQadaa_Code(String Qadaa_Code) {
        this.Qadaa_Code = Qadaa_Code;
    }

    public String getRegion_Code() {
        return Region_Code;
    }

    public void setRegion_Code(String Region_Code) {
        this.Region_Code = Region_Code;
    }

    public String getSq_Meter_Area() {
        return Sq_Meter_Area;
    }

    public void setSq_Meter_Area(String Sq_Meter_Area) {
        this.Sq_Meter_Area = Sq_Meter_Area;
    }

    public String getStamp_Date() {
        return Stamp_Date;
    }

    public void setStamp_Date(String Stamp_Date) {
        this.Stamp_Date = Stamp_Date;
    }

    public String getTax_Code() {
        return Tax_Code;
    }

    public void setTax_Code(String Tax_Code) {
        this.Tax_Code = Tax_Code;
    }

    public String getUser_Code() {
        return User_Code;
    }

    public void setUser_Code(String User_Code) {
        this.User_Code = User_Code;
    }

    public String getVAT_Number() {
        return VAT_Number;
    }

    public void setVAT_Number(String VAT_Number) {
        this.VAT_Number = VAT_Number;
    }

    public Object getYear_Of_Service() {
        return Year_Of_Service;
    }

    public void setYear_Of_Service(Object Year_Of_Service) {
        this.Year_Of_Service = Year_Of_Service;
    }
}
