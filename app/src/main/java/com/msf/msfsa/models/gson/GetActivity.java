package com.msf.msfsa.models.gson;

import com.msf.msfsa.events.ProgressBarStatus;
import com.msf.msfsa.events.SaveComplete;
import com.msf.msfsa.models.Activity;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 3/26/2016.
 */
public class GetActivity {
    List<Activity> GetActivitiesResult = new ArrayList<Activity>();

    public GetActivity(List<Activity> getAct_activityResult) {
        GetActivitiesResult = getAct_activityResult;
    }

    public GetActivity() {
    }

    public List<Activity> getGetAct_activityResult() {
        return GetActivitiesResult;
    }

    public void setGetAct_activityResult(List<Activity> getAct_activityResult) {
        GetActivitiesResult = getAct_activityResult;
    }

    public void save() {
        for (int i = 0; i < this.GetActivitiesResult.size(); i++) {
            this.GetActivitiesResult.get(i).save();
            EventBus.getDefault().post(new ProgressBarStatus(i));
        }
        EventBus.getDefault().post(new SaveComplete());
    }

    public int getSize() {
        return GetActivitiesResult.size();
    }
}
