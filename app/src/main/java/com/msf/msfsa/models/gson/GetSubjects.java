package com.msf.msfsa.models.gson;

import com.msf.msfsa.events.ProgressBarStatus;
import com.msf.msfsa.events.SaveComplete;
import com.msf.msfsa.models.Subjects;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 3/26/2016.
 */
public class GetSubjects {
    List<Subjects> GetAct_SubjectsResult = new ArrayList<>();

    public GetSubjects() {
    }

    public GetSubjects(List<Subjects> list) {
        this.GetAct_SubjectsResult = list;
    }

    public List<Subjects> getList() {
        return GetAct_SubjectsResult;
    }

    public void setList(List<Subjects> list) {
        this.GetAct_SubjectsResult = list;
    }

    public void save() {
        for (int i = 0; i < this.GetAct_SubjectsResult.size(); i++) {
            this.GetAct_SubjectsResult.get(i).save();
            EventBus.getDefault().post(new ProgressBarStatus(i));

        }
        EventBus.getDefault().post(new SaveComplete());
    }

    public int getSize() {
        return GetAct_SubjectsResult.size();
    }
}
