package com.msf.msfsa.models;

import com.orm.SugarRecord;

/**
 * Created by Kamil on 5/15/2016.
 */
public class Visits extends SugarRecord {
    /**
     * Visit_ID : 20120522752000
     * Org_ID : 1
     * Journey_Code : 61520120522064500
     * User_Code : 615
     * Client_Code : 615
     * Census_Client_Code : Null
     * Start_Date : 5/22/12 7:52
     * End_Date : 5/22/12 8:12
     * Is_Invoice : NULL
     * Is_Order : NULL
     * Is_FOC : NULL
     * Is_Return : NULL
     * Is_Credit_Note : NULL
     * Is_Payment : NULL
     * Is_Merchandizing : NULL
     * No_Trx_Reason_Code : NULL
     * Visit_Reason_Code : NULL
     * Is_Processed : NULL
     * Longitude : 22.12345679
     * Latitude : 38.12345679
     * Suggested_Time : NULL
     * Visit_Type : NULL
     * Visit_Status : NULL
     * Stamp_Date : From Server
     * Export_To_JDE : NULL
     * No_Merchandizing_Reason_Code : NULL
     * No_Item_Distribution_Reason_Code : NULL
     * SyncWithERP : NULL
     * Route_ID : NULL
     * Core_Distance : 0
     */

    private long Visit_ID;
    private int Org_ID;
    private long Journey_Code;
    private int User_Code;
    private String Client_Code;
    private String Census_Client_Code;
    private String Start_Date;
    private String End_Date;
    private String Is_Invoice;
    private String Is_Order;
    private String Is_FOC;
    private String Is_Return;
    private String Is_Credit_Note;
    private String Is_Payment;
    private String Is_Merchandizing;
    private String No_Trx_Reason_Code;
    private String Visit_Reason_Code;
    private String Is_Processed;
    private double Longitude;
    private double Latitude;
    private String Suggested_Time;
    private String Visit_Type;
    private String Visit_Status;
    private String Stamp_Date;
    private String Export_To_JDE;
    private String No_Merchandizing_Reason_Code;
    private String No_Item_Distribution_Reason_Code;
    private String SyncWithERP;
    private String Route_ID;
    private String Core_Distance;

    public long getVisit_ID() {
        return Visit_ID;
    }

    public void setVisit_ID(long Visit_ID) {
        this.Visit_ID = Visit_ID;
    }

    public int getOrg_ID() {
        return Org_ID;
    }

    public void setOrg_ID(int Org_ID) {
        this.Org_ID = Org_ID;
    }

    public long getJourney_Code() {
        return Journey_Code;
    }

    public void setJourney_Code(long Journey_Code) {
        this.Journey_Code = Journey_Code;
    }

    public int getUser_Code() {
        return User_Code;
    }

    public void setUser_Code(int User_Code) {
        this.User_Code = User_Code;
    }

    public String getClient_Code() {
        return Client_Code;
    }

    public void setClient_Code(String Client_Code) {
        this.Client_Code = Client_Code;
    }

    public String getCensus_Client_Code() {
        return Census_Client_Code;
    }

    public void setCensus_Client_Code(String Census_Client_Code) {
        this.Census_Client_Code = Census_Client_Code;
    }

    public String getStart_Date() {
        return Start_Date;
    }

    public void setStart_Date(String Start_Date) {
        this.Start_Date = Start_Date;
    }

    public String getEnd_Date() {
        return End_Date;
    }

    public void setEnd_Date(String End_Date) {
        this.End_Date = End_Date;
    }

    public String getIs_Invoice() {
        return Is_Invoice;
    }

    public void setIs_Invoice(String Is_Invoice) {
        this.Is_Invoice = Is_Invoice;
    }

    public String getIs_Order() {
        return Is_Order;
    }

    public void setIs_Order(String Is_Order) {
        this.Is_Order = Is_Order;
    }

    public String getIs_FOC() {
        return Is_FOC;
    }

    public void setIs_FOC(String Is_FOC) {
        this.Is_FOC = Is_FOC;
    }

    public String getIs_Return() {
        return Is_Return;
    }

    public void setIs_Return(String Is_Return) {
        this.Is_Return = Is_Return;
    }

    public String getIs_Credit_Note() {
        return Is_Credit_Note;
    }

    public void setIs_Credit_Note(String Is_Credit_Note) {
        this.Is_Credit_Note = Is_Credit_Note;
    }

    public String getIs_Payment() {
        return Is_Payment;
    }

    public void setIs_Payment(String Is_Payment) {
        this.Is_Payment = Is_Payment;
    }

    public String getIs_Merchandizing() {
        return Is_Merchandizing;
    }

    public void setIs_Merchandizing(String Is_Merchandizing) {
        this.Is_Merchandizing = Is_Merchandizing;
    }

    public String getNo_Trx_Reason_Code() {
        return No_Trx_Reason_Code;
    }

    public void setNo_Trx_Reason_Code(String No_Trx_Reason_Code) {
        this.No_Trx_Reason_Code = No_Trx_Reason_Code;
    }

    public String getVisit_Reason_Code() {
        return Visit_Reason_Code;
    }

    public void setVisit_Reason_Code(String Visit_Reason_Code) {
        this.Visit_Reason_Code = Visit_Reason_Code;
    }

    public String getIs_Processed() {
        return Is_Processed;
    }

    public void setIs_Processed(String Is_Processed) {
        this.Is_Processed = Is_Processed;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double Longitude) {
        this.Longitude = Longitude;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double Latitude) {
        this.Latitude = Latitude;
    }

    public String getSuggested_Time() {
        return Suggested_Time;
    }

    public void setSuggested_Time(String Suggested_Time) {
        this.Suggested_Time = Suggested_Time;
    }

    public String getVisit_Type() {
        return Visit_Type;
    }

    public void setVisit_Type(String Visit_Type) {
        this.Visit_Type = Visit_Type;
    }

    public String getVisit_Status() {
        return Visit_Status;
    }

    public void setVisit_Status(String Visit_Status) {
        this.Visit_Status = Visit_Status;
    }

    public String getStamp_Date() {
        return Stamp_Date;
    }

    public void setStamp_Date(String Stamp_Date) {
        this.Stamp_Date = Stamp_Date;
    }

    public String getExport_To_JDE() {
        return Export_To_JDE;
    }

    public void setExport_To_JDE(String Export_To_JDE) {
        this.Export_To_JDE = Export_To_JDE;
    }

    public String getNo_Merchandizing_Reason_Code() {
        return No_Merchandizing_Reason_Code;
    }

    public void setNo_Merchandizing_Reason_Code(String No_Merchandizing_Reason_Code) {
        this.No_Merchandizing_Reason_Code = No_Merchandizing_Reason_Code;
    }

    public String getNo_Item_Distribution_Reason_Code() {
        return No_Item_Distribution_Reason_Code;
    }

    public void setNo_Item_Distribution_Reason_Code(String No_Item_Distribution_Reason_Code) {
        this.No_Item_Distribution_Reason_Code = No_Item_Distribution_Reason_Code;
    }

    public String getSyncWithERP() {
        return SyncWithERP;
    }

    public void setSyncWithERP(String SyncWithERP) {
        this.SyncWithERP = SyncWithERP;
    }

    public String getRoute_ID() {
        return Route_ID;
    }

    public void setRoute_ID(String Route_ID) {
        this.Route_ID = Route_ID;
    }

    public String getCore_Distance() {
        return Core_Distance;
    }

    public void setCore_Distance(String Core_Distance) {
        this.Core_Distance = Core_Distance;
    }
}
