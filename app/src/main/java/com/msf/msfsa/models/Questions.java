package com.msf.msfsa.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by Kamil on 3/25/2016.
 */
public class Questions extends SugarRecord implements Parcelable {

    public static final Creator<Questions> CREATOR = new Creator<Questions>() {
        @Override
        public Questions createFromParcel(Parcel source) {
            return new Questions(source);
        }

        @Override
        public Questions[] newArray(int size) {
            return new Questions[size];
        }
    };
    /**
     * Activity_ID : 20120304114150
     * Max_Value : null
     * Min_Value : null
     * Question_Alt_Description : Do this outlet sells Parliament ?
     * Question_Description : Do this outlet sells Parliament ?
     * Question_ID : 20120304114710
     * Question_Type : 2
     * Stamp_Date : /Date(1330850842030+0300)/
     */

    private long Activity_ID;
    private int Max_Value;
    private int Min_Value;
    private String Question_Alt_Description;
    private String Question_Description;
    private long Question_ID;
    private int Question_Type;
    private String Stamp_Date;

    public Questions() {
    }

    protected Questions(Parcel in) {
        this.Activity_ID = in.readLong();
        this.Max_Value = in.readInt();
        this.Min_Value = in.readInt();
        this.Question_Alt_Description = in.readString();
        this.Question_Description = in.readString();
        this.Question_ID = in.readLong();
        this.Question_Type = in.readInt();
        this.Stamp_Date = in.readString();
    }

    public long getActivity_ID() {
        return Activity_ID;
    }

    public void setActivity_ID(long Activity_ID) {
        this.Activity_ID = Activity_ID;
    }

    public int getMax_Value() {
        return Max_Value;
    }

    public void setMax_Value(int Max_Value) {
        this.Max_Value = Max_Value;
    }

    public int getMin_Value() {
        return Min_Value;
    }

    public void setMin_Value(int Min_Value) {
        this.Min_Value = Min_Value;
    }

    public String getQuestion_Alt_Description() {
        return Question_Alt_Description;
    }

    public void setQuestion_Alt_Description(String Question_Alt_Description) {
        this.Question_Alt_Description = Question_Alt_Description;
    }

    public String getQuestion_Description() {
        return Question_Description;
    }

    public void setQuestion_Description(String Question_Description) {
        this.Question_Description = Question_Description;
    }

    public long getQuestion_ID() {
        return Question_ID;
    }

    public void setQuestion_ID(long Question_ID) {
        this.Question_ID = Question_ID;
    }

    public int getQuestion_Type() {
        return Question_Type;
    }

    public void setQuestion_Type(int Question_Type) {
        this.Question_Type = Question_Type;
    }

    public String getStamp_Date() {
        return Stamp_Date;
    }

    public void setStamp_Date(String Stamp_Date) {
        this.Stamp_Date = Stamp_Date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.Activity_ID);
        dest.writeInt(this.Max_Value);
        dest.writeInt(this.Min_Value);
        dest.writeString(this.Question_Alt_Description);
        dest.writeString(this.Question_Description);
        dest.writeLong(this.Question_ID);
        dest.writeInt(this.Question_Type);
        dest.writeString(this.Stamp_Date);
    }
}
