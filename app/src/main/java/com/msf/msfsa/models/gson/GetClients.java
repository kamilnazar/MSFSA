package com.msf.msfsa.models.gson;

import com.msf.msfsa.events.ProgressBarStatus;
import com.msf.msfsa.events.SaveComplete;
import com.msf.msfsa.models.Clients;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 3/26/2016.
 */
public class GetClients {
    private List<Clients> GetClientsResult = new ArrayList<>();

    public GetClients() {
    }

    public GetClients(List<Clients> clientsList) {
        this.GetClientsResult = clientsList;
    }

    public List<Clients> getClientsList() {
        return GetClientsResult;
    }

    public void setClientsList(List<Clients> clientsList) {
        this.GetClientsResult = clientsList;
    }

    public void save() {
        for (int i = 0; i < this.GetClientsResult.size(); i++) {
            this.GetClientsResult.get(i).save();

            EventBus.getDefault().post(new ProgressBarStatus(i));
        }
        EventBus.getDefault().post(new SaveComplete());
    }

    public int getSize() {
        return GetClientsResult.size();
    }
}
