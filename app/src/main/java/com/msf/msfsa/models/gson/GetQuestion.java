package com.msf.msfsa.models.gson;

import com.msf.msfsa.events.ProgressBarStatus;
import com.msf.msfsa.events.SaveComplete;
import com.msf.msfsa.models.Questions;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 3/26/2016.
 */
public class GetQuestion {
    private List<Questions> GetAct_QuestionsResult = new ArrayList<>();

    public GetQuestion() {
    }

    public GetQuestion(List<Questions> questionsList) {
        this.GetAct_QuestionsResult = questionsList;
    }

    public List<Questions> getQuestionsList() {
        return GetAct_QuestionsResult;
    }

    public void setQuestionsList(List<Questions> questionsList) {
        this.GetAct_QuestionsResult = questionsList;
    }

    public void save() {
        for (int i = 0; i < this.GetAct_QuestionsResult.size(); i++) {
            this.GetAct_QuestionsResult.get(i).save();
            EventBus.getDefault().post(new ProgressBarStatus(i));

        }
        EventBus.getDefault().post(new SaveComplete());
    }

    public int getSize() {
        return GetAct_QuestionsResult.size();
    }
}
