package com.msf.msfsa.models.gson;

import com.msf.msfsa.events.ProgressBarStatus;
import com.msf.msfsa.events.SaveComplete;
import com.msf.msfsa.models.Options;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Kamil on 4/15/2016.
 */
public class GetOptions {
    /**
     * Activity_ID : 100002
     * Option_Alt_Description : خهات 1
     * Option_Description : Opt  1
     * Option_ID : 1
     * Question_ID : 1
     * Stamp_Date : /Date(1460025006383+0300)/
     */

    private List<Options> GetAct_OptionsResult;

    public List<Options> getGetAct_OptionsResult() {
        return GetAct_OptionsResult;
    }

    public void setGetAct_OptionsResult(List<Options> GetAct_OptionsResult) {
        this.GetAct_OptionsResult = GetAct_OptionsResult;
    }

    public void save() {
        for (int i = 0; i < this.GetAct_OptionsResult.size(); i++) {
            this.GetAct_OptionsResult.get(i).save();
            EventBus.getDefault().post(new ProgressBarStatus(i));

        }
        EventBus.getDefault().post(new SaveComplete());
    }

    public int getSize() {
        return GetAct_OptionsResult.size();
    }
}
