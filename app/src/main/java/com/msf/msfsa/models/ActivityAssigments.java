package com.msf.msfsa.models;

import com.orm.SugarRecord;

/**
 * Created by Kamil on 3/25/2016.
 */
public class ActivityAssigments extends SugarRecord {

    /**
     * Activity_ID : 20140326085931
     * Merchandiser_Code : 221
     * Merchandiser_Type : 0
     * Org_ID : 1
     * Stamp_Date : /Date(1395781200000+0300)/
     */

    private long Activity_ID;
    private String Merchandiser_Code;
    private String Merchandiser_Type;
    private String Org_ID;
    private String Stamp_Date;

    public long getActivity_ID() {
        return Activity_ID;
    }

    public void setActivity_ID(long Activity_ID) {
        this.Activity_ID = Activity_ID;
    }

    public String getMerchandiser_Code() {
        return Merchandiser_Code;
    }

    public void setMerchandiser_Code(String Merchandiser_Code) {
        this.Merchandiser_Code = Merchandiser_Code;
    }

    public String getMerchandiser_Type() {
        return Merchandiser_Type;
    }

    public void setMerchandiser_Type(String Merchandiser_Type) {
        this.Merchandiser_Type = Merchandiser_Type;
    }

    public String getOrg_ID() {
        return Org_ID;
    }

    public void setOrg_ID(String Org_ID) {
        this.Org_ID = Org_ID;
    }

    public String getStamp_Date() {
        return Stamp_Date;
    }

    public void setStamp_Date(String Stamp_Date) {
        this.Stamp_Date = Stamp_Date;
    }
}
