package com.msf.msfsa.models;

import com.orm.SugarRecord;

/**
 * Created by Kamil on 4/24/2016.
 */
public class Answer extends SugarRecord {
    /**
     * Line_ID : 1
     * User_Code : 28
     * Org_ID : 1
     * Activity_ID : 2
     * Subject_Code : 11
     * Question_ID : 1
     * Answer : No anwer
     * Client_Code : 200202
     * Answer_Date : NULL
     * Journey_Code : 2820120521031700
     * Visit_ID : NULL
     * Is_Processed : NULL
     * Stamp_Date : 06:22.8
     * SyncWithERP : NULL
     */

    private long Line_ID;
    private int User_Code;
    private int Org_ID;
    private int Activity_ID;
    private int Subject_Code;
    private long Question_ID;
    private String Answer;
    private int Client_Code;
    private String Answer_Date;
    private long Journey_Code;
    private long Visit_ID;
    private int Is_Processed;
    private String Stamp_Date;
    private int SyncWithERP;

    public long getLine_ID() {
        return Line_ID;
    }

    public void setLine_ID(long Line_ID) {
        this.Line_ID = Line_ID;
    }

    public int getUser_Code() {
        return User_Code;
    }

    public void setUser_Code(int User_Code) {
        this.User_Code = User_Code;
    }

    public int getOrg_ID() {
        return Org_ID;
    }

    public void setOrg_ID(int Org_ID) {
        this.Org_ID = Org_ID;
    }

    public int getActivity_ID() {
        return Activity_ID;
    }

    public void setActivity_ID(int Activity_ID) {
        this.Activity_ID = Activity_ID;
    }

    public int getSubject_Code() {
        return Subject_Code;
    }

    public void setSubject_Code(int Subject_Code) {
        this.Subject_Code = Subject_Code;
    }

    public long getQuestion_ID() {
        return Question_ID;
    }

    public void setQuestion_ID(long Question_ID) {
        this.Question_ID = Question_ID;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String Answer) {
        this.Answer = Answer;
    }

    public int getClient_Code() {
        return Client_Code;
    }

    public void setClient_Code(int Client_Code) {
        this.Client_Code = Client_Code;
    }

    public String getAnswer_Date() {
        return Answer_Date;
    }

    public void setAnswer_Date(String Answer_Date) {
        this.Answer_Date = Answer_Date;
    }

    public long getJourney_Code() {
        return Journey_Code;
    }

    public void setJourney_Code(long Journey_Code) {
        this.Journey_Code = Journey_Code;
    }

    public long getVisit_ID() {
        return Visit_ID;
    }

    public void setVisit_ID(long Visit_ID) {
        this.Visit_ID = Visit_ID;
    }

    public int getIs_Processed() {
        return Is_Processed;
    }

    public void setIs_Processed(int Is_Processed) {
        this.Is_Processed = Is_Processed;
    }

    public String getStamp_Date() {
        return Stamp_Date;
    }

    public void setStamp_Date(String Stamp_Date) {
        this.Stamp_Date = Stamp_Date;
    }

    public int getSyncWithERP() {
        return SyncWithERP;
    }

    public void setSyncWithERP(int SyncWithERP) {
        this.SyncWithERP = SyncWithERP;
    }
//    @Override
//    public void save(){
//        super.save();
//    }
}
