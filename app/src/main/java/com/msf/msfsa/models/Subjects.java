package com.msf.msfsa.models;

import com.orm.SugarRecord;

/**
 * Created by Kamil on 3/25/2016.
 */
public class Subjects extends SugarRecord {

    /**
     * Activity_ID : 20120304114150
     * Stamp_Date : /Date(1330850842010+0300)/
     * Subject_Alt_Description : بـــقــالــه
     * Subject_Code : 1
     * Subject_Description : BAQALA
     */

    private long Activity_ID;
    private String Stamp_Date;
    private String Subject_Alt_Description;
    private String Subject_Code;
    private String Subject_Description;

    public long getActivity_ID() {
        return Activity_ID;
    }

    public void setActivity_ID(long Activity_ID) {
        this.Activity_ID = Activity_ID;
    }

    public String getStamp_Date() {
        return Stamp_Date;
    }

    public void setStamp_Date(String Stamp_Date) {
        this.Stamp_Date = Stamp_Date;
    }

    public String getSubject_Alt_Description() {
        return Subject_Alt_Description;
    }

    public void setSubject_Alt_Description(String Subject_Alt_Description) {
        this.Subject_Alt_Description = Subject_Alt_Description;
    }

    public String getSubject_Code() {
        return Subject_Code;
    }

    public void setSubject_Code(String Subject_Code) {
        this.Subject_Code = Subject_Code;
    }

    public String getSubject_Description() {
        return Subject_Description;
    }

    public void setSubject_Description(String Subject_Description) {
        this.Subject_Description = Subject_Description;
    }
}
