package com.msf.msfsa.models;

import com.orm.SugarRecord;

/**
 * Created by Kamil on 4/13/2016.
 */
public class Options extends SugarRecord {

    /**
     * Activity_ID : 100002
     * Option_Alt_Description : خهات 1
     * Option_Description : Opt  1
     * Option_ID : 1
     * Question_ID : 1
     * Stamp_Date : /Date(1460025006383+0300)/
     */

    private int Activity_ID;
    private String Option_Alt_Description;
    private String Option_Description;
    private int Option_ID;
    private int Question_ID;
    private String Stamp_Date;

    public int getActivity_ID() {
        return Activity_ID;
    }

    public void setActivity_ID(int Activity_ID) {
        this.Activity_ID = Activity_ID;
    }

    public String getOption_Alt_Description() {
        return Option_Alt_Description;
    }

    public void setOption_Alt_Description(String Option_Alt_Description) {
        this.Option_Alt_Description = Option_Alt_Description;
    }

    public String getOption_Description() {
        return Option_Description;
    }

    public void setOption_Description(String Option_Description) {
        this.Option_Description = Option_Description;
    }

    public int getOption_ID() {
        return Option_ID;
    }

    public void setOption_ID(int Option_ID) {
        this.Option_ID = Option_ID;
    }

    public int getQuestion_ID() {
        return Question_ID;
    }

    public void setQuestion_ID(int Question_ID) {
        this.Question_ID = Question_ID;
    }

    public String getStamp_Date() {
        return Stamp_Date;
    }

    public void setStamp_Date(String Stamp_Date) {
        this.Stamp_Date = Stamp_Date;
    }
}
