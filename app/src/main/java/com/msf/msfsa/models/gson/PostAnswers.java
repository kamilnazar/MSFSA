package com.msf.msfsa.models.gson;

import com.msf.msfsa.models.Answer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 4/24/2016.
 */
public class PostAnswers {
    List<Answer> Act_Answers = new ArrayList<>();

    public PostAnswers() {
    }

    public PostAnswers(List<Answer> setAct_AnswersResult) {
        Act_Answers = setAct_AnswersResult;
    }

    public List<Answer> getSetAct_AnswersResult() {
        return Act_Answers;
    }

    public void setSetAct_AnswersResult(List<Answer> setAct_AnswersResult) {
        Act_Answers = setAct_AnswersResult;
    }

    public int getSize() {
        return Act_Answers.size();
    }
}
