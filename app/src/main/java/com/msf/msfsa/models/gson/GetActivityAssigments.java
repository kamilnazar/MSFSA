package com.msf.msfsa.models.gson;

import com.msf.msfsa.events.ProgressBarStatus;
import com.msf.msfsa.events.SaveComplete;
import com.msf.msfsa.models.ActivityAssigments;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 3/26/2016.
 */
public class GetActivityAssigments {

    private List<ActivityAssigments> GetActivity_AssignmentsResult = new ArrayList<>();

    public GetActivityAssigments(List<ActivityAssigments> activityAssigmentsList) {
        this.GetActivity_AssignmentsResult = activityAssigmentsList;
    }


    public GetActivityAssigments() {
    }

    public List<ActivityAssigments> getActivityAssigmentsList() {
        return GetActivity_AssignmentsResult;
    }

    public void setActivityAssigmentsList(List<ActivityAssigments> activityAssigmentsList) {
        this.GetActivity_AssignmentsResult = activityAssigmentsList;
    }

    public void save() {
        for (int i = 0; i < this.GetActivity_AssignmentsResult.size(); i++) {
            this.GetActivity_AssignmentsResult.get(i).save();
            EventBus.getDefault().post(new ProgressBarStatus(i));
        }
        EventBus.getDefault().post(new SaveComplete());
    }

    public int getSize() {
        return GetActivity_AssignmentsResult.size();
    }
}
