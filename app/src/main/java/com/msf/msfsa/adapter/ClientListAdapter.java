package com.msf.msfsa.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.msf.msfsa.R;
import com.msf.msfsa.activity.QuestionListActivity;
import com.msf.msfsa.models.Answer;
import com.msf.msfsa.models.Clients;
import com.msf.msfsa.models.Questions;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 3/28/2016.
 */
public class ClientListAdapter extends RecyclerView.Adapter<ClientListAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    private final List<Clients> mModels;
    Context context;
    int activityid;

    public ClientListAdapter(Context context, List<Clients> models, int id) {
        mInflater = LayoutInflater.from(context);
        mModels = new ArrayList<>(models);
        this.context = context;
        this.activityid = id;
    }

    @Override
    public ClientListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.client_list_item,
                parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        String text = mModels.get(position).getClient_Code() + " - " + mModels.get(position).getClient_Description();
        holder.name_text.setText(text);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, QuestionListActivity.class);
                intent.putExtra("activity_id", activityid);
                intent.putExtra("client_id", mModels.get(position).getClient_Code());
                context.startActivity(intent);
            }
        });
        long dbcount = Select.from(Answer.class)
                .where(Condition.prop("activityid").eq(activityid),
                        Condition.prop("clientcode").eq(mModels.get(position).getClient_Code()))
                .count();
        long qs_count = Questions.find(Questions.class, "activityid = ?", String.valueOf(activityid)).size();
        if (dbcount == qs_count) {
            holder.tick_green.setVisibility(View.VISIBLE);
            holder.tick_orange.setVisibility(View.INVISIBLE);
        } else if (dbcount == 0) {
            holder.tick_green.setVisibility(View.INVISIBLE);
            holder.tick_orange.setVisibility(View.INVISIBLE);
        } else {
            holder.tick_green.setVisibility(View.INVISIBLE);
            holder.tick_orange.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mModels.size();
    }

    public void animateTo(List<Clients> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    private void applyAndAnimateRemovals(List<Clients> newModels) {
        for (int i = mModels.size() - 1; i >= 0; i--) {
            final Clients model = mModels.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }

    private void applyAndAnimateAdditions(List<Clients> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final Clients model = newModels.get(i);
            if (!mModels.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<Clients> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final Clients model = newModels.get(toPosition);
            final int fromPosition = mModels.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public Clients removeItem(int position) {
        final Clients model = mModels.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, Clients model) {
        mModels.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final Clients model = mModels.remove(fromPosition);
        mModels.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView name_text;
        public ImageView tick_green, tick_orange;

        public ViewHolder(View itemView) {
            super(itemView);
            name_text = (TextView) itemView.findViewById(R.id.client_text);
            tick_green = (ImageView) itemView.findViewById(R.id.ic_done_green);
            tick_orange = (ImageView) itemView.findViewById(R.id.ic_done_orange);
        }
    }

}
