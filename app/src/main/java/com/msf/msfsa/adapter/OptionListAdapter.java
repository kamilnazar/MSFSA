package com.msf.msfsa.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.msf.msfsa.R;
import com.msf.msfsa.models.Options;

import java.util.List;

/**
 * Created by Kamil on 4/15/2016.
 */
public class OptionListAdapter extends ArrayAdapter<Options> {
    private final Activity context;
    private final List<Options> names;

    public OptionListAdapter(Activity context, List<Options> names) {
        super(context, R.layout.option_item_layout, names);
        this.context = context;
        this.names = names;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.option_item_layout, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) rowView.findViewById(R.id.option_text);
            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        String s = names.get(position).getOption_Description();
        holder.text.setText(s);
        return rowView;
    }

    static class ViewHolder {
        public TextView text;
    }
}

