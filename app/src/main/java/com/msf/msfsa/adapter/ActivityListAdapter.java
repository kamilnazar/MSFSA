package com.msf.msfsa.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.msf.msfsa.R;
import com.msf.msfsa.activity.ClientListActivity;
import com.msf.msfsa.models.Activity;

import java.util.List;

/**
 * Created by Kamil on 4/8/2016.
 */
public class ActivityListAdapter extends RecyclerView.Adapter<ActivityListAdapter.ViewHolder> {

    private final LayoutInflater mInflater;
    List<Activity> activities;
    Context context;

    public ActivityListAdapter(Context context, List<Activity> activityList) {
        this.activities = activityList;
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        Log.e("activity count", String.valueOf(activityList.size()));
    }

    @Override
    public ActivityListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.drawer_list_item,
                parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ActivityListAdapter.ViewHolder holder, final int position) {
        String text = activities.get(position).getActivity_Description();
        holder.name.setText(text);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ClientListActivity.class);
                intent.putExtra("activity_id", activities.get(position).getActivity_ID());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.groupName);
        }
    }
}
