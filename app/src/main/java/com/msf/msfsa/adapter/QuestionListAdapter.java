package com.msf.msfsa.adapter;

/**
 * Created by Kamil on 4/19/2016.
 */


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.msf.msfsa.R;
import com.msf.msfsa.activity.QuestionContainer;
import com.msf.msfsa.models.Answer;
import com.msf.msfsa.models.Questions;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

/**
 * Created by Kamil on 4/7/2016.
 */
public class QuestionListAdapter extends RecyclerView.Adapter<QuestionListAdapter.ViewHolder> {
    List<Questions> questionsList;
    Context context;
    int activity_id;
    String client_id;

    public QuestionListAdapter(Context context, List<Questions> questionsList, String clientid, int activityid) {
        this.questionsList = questionsList;
        this.context = context;
        this.client_id = clientid;
        this.activity_id = activityid;
    }

    @Override
    public QuestionListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.question_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionListAdapter.ViewHolder holder, final int position) {
        String text = questionsList.get(position).getQuestion_Description();
//        Log.e("group id", String.valueOf(group_id));
        holder.question.setText(text);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, QuestionContainer.class);
                intent.putExtra("activity_id", activity_id);
                intent.putExtra("client_id", client_id);
                intent.putExtra("question", questionsList.get(position));
                context.startActivity(intent);
            }
        });

        long count = Select.from(Answer.class)
                .where(Condition.prop("activityid").eq(activity_id),
                        Condition.prop("clientcode").eq(client_id),
                        Condition.prop("questionid").eq(questionsList.get(position).getQuestion_ID()))
                .count();
        if (count == 0) {
            holder.tick.setVisibility(View.INVISIBLE);
        } else {
            holder.tick.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public int getItemCount() {
        return questionsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView question;
        ImageView mThumbnailImage, tick;

        public ViewHolder(View itemView) {
            super(itemView);
            question = (TextView) itemView.findViewById(R.id.question);
            mThumbnailImage = (ImageView) itemView.findViewById(R.id.thumbnail_image);
            tick = (ImageView) itemView.findViewById(R.id.ic_done);
        }
    }
}
