package com.msf.msfsa.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.msf.msfsa.R;
import com.msf.msfsa.activity.GroupSelectActivity;
import com.msf.msfsa.fragment.ActivityListFragment;
import com.msf.msfsa.models.Groups;

import java.util.List;

/**
 * Created by Kamil on 4/7/2016.
 */
public class GroupListAdapter extends RecyclerView.Adapter<GroupListAdapter.ViewHolder> {
    List<Groups> groupsList;
    Context context;

    public GroupListAdapter(Context context, List<Groups> groupsList) {
        this.groupsList = groupsList;
        this.context = context;
    }

    @Override
    public GroupListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.drawer_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GroupListAdapter.ViewHolder holder, final int position) {
        String text = groupsList.get(position).getAct_Group_Description();
        final int group_id = groupsList.get(position).getAct_Group_Code();
//        Log.e("group id", String.valueOf(group_id));
        holder.groupname.setText(text);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = (Fragment) ActivityListFragment.newInstance(group_id);
                GroupSelectActivity groupSelectActivity = (GroupSelectActivity) context;
                groupSelectActivity.switchFragment(fragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return groupsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView groupname;

        public ViewHolder(View itemView) {
            super(itemView);
            groupname = (TextView) itemView.findViewById(R.id.groupName);
        }
    }
}
