package com.msf.msfsa.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by Kamil on 5/15/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context arg0, Intent arg1) {
        // For our recurring task, we'll just display a message
        Toast.makeText(arg0, "Uploading locations", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(arg0,VisitUploadService.class);
        arg0.startService(intent);
    }

}