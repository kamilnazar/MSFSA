package com.msf.msfsa.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.msf.msfsa.application.MyApplication;
import com.msf.msfsa.models.Visits;
import com.msf.msfsa.models.gson.SetVisit;
import com.msf.msfsa.network.EndPoints;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kamil on 5/15/2016.
 */
public class VisitUploadService extends Service {
    String TAG = getClass().getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "upload service started");
        Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
        if (Visits.listAll(Visits.class).size() > 0) {
            try {
//                JSONObject data = new JSONObject(new Gson().toJson(new SetVisit(Visits.listAll(Visits.class))));
                Log.e(TAG,gson.toJson(new SetVisit(Visits.listAll(Visits.class))));
                JSONObject data  = new JSONObject(gson.toJson(new SetVisit(Visits.listAll(Visits.class))));
                upload(data);
            } catch (JSONException e) {

            }
        }else this.stopSelf();
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    void upload(JSONObject uploaddata) {
        JSONObject savedata = new JSONObject();
        try {
            uploaddata.put("User_Code", MyApplication.getInstance().getPrefManager().getUserid());
            savedata.put("User_Code", String.valueOf(MyApplication.getInstance().getPrefManager().getUserid()));
        } catch (JSONException e) {
        }

        Log.e(TAG,uploaddata.toString());
        Log.e(TAG,savedata.toString());

        final JsonObjectRequest save = new JsonObjectRequest(Request.Method.POST, EndPoints.SAVE_ANS, savedata, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    if (response.getBoolean("SaveDataResult")) {
                        Visits.deleteAll(Visits.class);
                        Toast.makeText(getApplicationContext(), "location uploaded", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Log.e(TAG,e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                stopSelf();
            }
        });



        JsonObjectRequest upload = new JsonObjectRequest(Request.Method.POST, EndPoints.SET_VISIT, uploaddata, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    if(response.getBoolean("SetVisitResult")){
                        MyApplication.getInstance().addToRequestQueue(save);
                    }
                }catch (JSONException e){

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                stopSelf();
            }
        });
        MyApplication.getInstance().addToRequestQueue(upload);
    }
}
