package com.msf.msfsa.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.msf.msfsa.application.MyApplication;
import com.msf.msfsa.models.Visits;

import java.util.Calendar;

import fr.quentinklein.slt.LocationTracker;
import fr.quentinklein.slt.TrackerSettings;

/**
 * Created by Kamil on 5/13/2016.
 */
public class BackgroundLocationService extends Service {
    LocationTracker tracker;
    String TAG = getClass().getSimpleName();

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "service created");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.d(TAG, "service started");
        Log.e(TAG,String.valueOf(MyApplication.getInstance().getPrefManager().getUserid()));
        if (MyApplication.getInstance().getPrefManager().getUserid()!=0) {
            if (isLocationEnabled(getApplicationContext())) {
                startTracking();
                Log.d(TAG, "tacking location");
            } else Log.e(TAG, "location not enabled");
        }else Log.e(TAG, "UserStatus false");


        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        tracker.stopListening();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    void startTracking() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // You need to ask the user to enable the permissions
            Log.d(TAG, "permissions not satisfied");
        } else {
            tracker = new LocationTracker(getApplicationContext(),
                    new TrackerSettings()
                            .setUseGPS(true)
                            .setUseNetwork(true)
                            .setUsePassive(true)
                            .setTimeBetweenUpdates(15 * 60 * 1000)) {
                @Override
                public void onLocationFound(@NonNull Location location) {
                    // Do some stuff
                    Calendar calendar = Calendar.getInstance();
                    Log.e(TAG, location.toString());
                    Visits visits = new Visits();
                    visits.setVisit_ID(MyApplication.getInstance().generateVisitID());
                    visits.setOrg_ID(1);
                    visits.setJourney_Code(MyApplication.getInstance().generateJourneyCode());
                    visits.setUser_Code(MyApplication.getInstance().getPrefManager().getUserid());
//                    visits.setClient_Code();
//                    visits.setCensus_Client_Code();
                    visits.setStart_Date("/" + "Date(" + String.valueOf(calendar.getTimeInMillis()) + ")" + "/");
                    visits.setEnd_Date("/" + "Date(" + String.valueOf(calendar.getTimeInMillis()) + ")" + "/");
//                    visits.setIs_Invoice();
//                    visits.setIs_Order();
//                    visits.setIs_Order();
                    visits.setLatitude(location.getLatitude());
                    visits.setLongitude(location.getLongitude());
                    visits.setStart_Date("/" + "Date(" + String.valueOf(calendar.getTimeInMillis()) + ")" + "/");
                    visits.setEnd_Date("/" + "Date(" + String.valueOf(calendar.getTimeInMillis()) + ")" + "/");
                    visits.save();
                }

                @Override
                public void onTimeout() {
                    if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // You need to ask the user to enable the permissions
                        Log.d(TAG, "permissions not satisfied");
                    } else {
                        tracker.startListening();
                    }
                }
            };
            tracker.startListening();
        }

    }
}
