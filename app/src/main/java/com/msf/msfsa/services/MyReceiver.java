package com.msf.msfsa.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.msf.msfsa.application.MyApplication;

/**
 * Created by Kamil on 5/13/2016.
 */
public class MyReceiver extends BroadcastReceiver {

    String TAG = getClass().getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {

        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            if (MyApplication.getInstance().getPrefManager().getUserstatus()) {

                Intent myIntent = new Intent(context, BackgroundLocationService.class);
                context.startService(myIntent);
                Log.v("TEST", "Service loaded at start");
            }

        } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {

            final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            final NetworkInfo ni = connectivityManager.getActiveNetworkInfo();

            if (ni != null && ni.isConnectedOrConnecting()) {

                Log.i(TAG, "Network " + ni.getTypeName() + " connected");

            } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {

                Log.d(TAG, "There's no network connectivity");

            }
        }
    }
}
