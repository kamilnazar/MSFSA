package com.msf.msfsa.application;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.msf.msfsa.services.BackgroundLocationService;

import java.util.Calendar;
//import com.orm.SugarContext;
//import com.orm.SugarContext;

/**
 * Created by Kamil on 3/25/2016.
 */
public class MyApplication extends com.orm.SugarApp {

    public static final String TAG = MyApplication.class
            .getSimpleName();
    private static MyApplication mInstance;
    private RequestQueue mRequestQueue;
    private MyPreferenceManager pref;
    private String android_id;

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        if (!isMyServiceRunning(BackgroundLocationService.class)) {
            Intent intent = new Intent(this, BackgroundLocationService.class);
            startService(intent);
        }
//        Stetho.initializeWithDefaults(this);
//        SugarContext.init(this);
    }

    public String getDeviceId() {
        if (android_id == null) {
            android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        }
        return android_id;
    }

    public MyPreferenceManager getPrefManager() {
        if (pref == null) {
            pref = new MyPreferenceManager(this);
        }
        return pref;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
        Log.d(TAG, "Added to queue" + req.getUrl());
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public long generateJourneyCode() {
        Calendar mCalendar;
        mCalendar = Calendar.getInstance();
        int mSec = mCalendar.get(Calendar.SECOND);
        int mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
        int mMinute = mCalendar.get(Calendar.MINUTE);
        int mYear = mCalendar.get(Calendar.YEAR);
        int mMonth = mCalendar.get(Calendar.MONTH) + 1;
        int mDay = mCalendar.get(Calendar.DATE);
        String journeycode = String.valueOf(pref.getUserid()) + mYear + mMonth + mDay + mHour + mMinute + mSec;
        Log.d("journey code", journeycode);
        return Long.valueOf(journeycode);
    }
    public long generateVisitID() {
        Calendar mCalendar;
        mCalendar = Calendar.getInstance();
        int mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
        int mMinute = mCalendar.get(Calendar.MINUTE);
        int mYear = mCalendar.get(Calendar.YEAR);
        int mMonth = mCalendar.get(Calendar.MONTH) + 1;
        int mDay = mCalendar.get(Calendar.DATE);
        String journeycode = String.valueOf(pref.getUserid()) + mYear + mMonth + mDay + mHour + mMinute;
        Log.d("journey code", journeycode);
        return Long.valueOf(journeycode);
    }

    public String getWifiMacAddress() {
        WifiManager mWifiManager;
        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        return mWifiManager.getConnectionInfo().getMacAddress();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}

