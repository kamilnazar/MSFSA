package com.msf.msfsa.application;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Kamil on 4/24/2016.
 */
public class MyPreferenceManager {

    // Sharedpref file name
    private static final String PREF_NAME = "msfsa";
    // All Shared Preferences Keys
    private static final String KEY_USER_LOGEDIN = "user_logedin";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_ISACTIVE = "is_active";
    // Shared Preferences
    SharedPreferences pref;
    // Editor for Shared preferences
    SharedPreferences.Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    private String TAG = MyPreferenceManager.class.getSimpleName();
//    private static final String KEY_USER_EMAIL = "user_email";
//    private static final String KEY_NOTIFICATIONS = "notifications";

    // Constructor
    public MyPreferenceManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean getUserstatus() {
        return pref.getBoolean(KEY_USER_LOGEDIN, false);
    }

    public void setUserstatus(boolean bool) {
        editor.putBoolean(KEY_USER_LOGEDIN, bool);
        editor.commit();
        Log.e(TAG, "User status is stored in shared preferences. ");
    }

    public void setUserId(int id) {
        editor.putInt(KEY_USER_ID, id);
        editor.commit();
        Log.e(TAG, "User key is stored in shared preferences. ");
    }

    public void setIsActive(boolean value) {
        editor.putBoolean(KEY_ISACTIVE, value);
        editor.commit();
        Log.e(TAG, "User  active. ");
    }

    public boolean isActive() {
        return pref.getBoolean(KEY_ISACTIVE, false);
    }

    public int getUserid() {
        if (pref.getInt(KEY_USER_ID, 0) != 0) {
            return pref.getInt(KEY_USER_ID, 0);
        }
        return 0;
    }

    public void clear() {
        editor.clear();
        editor.commit();
    }
}